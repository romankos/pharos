#ifndef serializable_h
#define serializable_h

#include <fstream>

/**  
 * Class Serializable provides interface for serializing an object to file.
 */
class Serializable {
public:
    virtual ~Serializable() {}
    
    /**
	 * Method toFile writes an object to file.
	 * 
	 * @param[in] file Output File Stream which objects is written to.
     * @return Ptrue if object is successfully written, false - otherwise.
	 */
    virtual bool toFile (std::ofstream& file) const = 0;
};

#endif