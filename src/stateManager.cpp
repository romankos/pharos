#include "stateManager.h"
#include "genericInputHandler.h"
#include "character.h"
#include "game.h"


StateManager::StateManager(const InputProvider* inputProvider, Renderer* renderer) : inputProvider(inputProvider), currRenderer(renderer) {}
StateManager::~StateManager() { 
    while (stateStack.size()) { delete stateStack.top(); stateStack.pop(); } 
    while (statesToDelete.size()) { delete statesToDelete.front(); statesToDelete.pop(); } 
    while (statesToInsert.size()) { delete statesToInsert.front(); statesToInsert.pop(); } 
    delete inputProvider;
    delete currRenderer;
}


void StateManager::update() {
    while (statesToInsert.size() > 0) { 
        push(statesToInsert.front()); 
        statesToInsert.pop();
    }
    while (statesToDelete.size() > 0) { 
        delete statesToDelete.front();
        statesToDelete.pop();
    }
    render();
    stateStack.top()->handleInput(inputProvider->getInput());
}
void StateManager::render() const { if(!isEmpty()) { stateStack.top()->render(); } }

void StateManager::push(Data* data, Renderer* renderer, InputHandler* inputHandler) {
    if(stateStack.size()) { stateStack.top()->exit(); }
    stateStack.push(new GameState(data, renderer, inputHandler));
    stateStack.top()->enter();
}
void StateManager::push(Data* data, InputHandler* inputHandler) {
    push(data, currRenderer->copy(), inputHandler);
}
void StateManager::push(GameState* state) {
    if(stateStack.size()) { stateStack.top()->exit(); }
    stateStack.push(state);
    stateStack.top()->enter();
}

void StateManager::pop() { 
    stateStack.top()->exit();
    statesToDelete.push(stateStack.top());
    stateStack.pop();
    if (!isEmpty()) { stateStack.top()->enter(); }
}

void StateManager::showInfo(Data* data) {
    push(data);
    render();
    pop();
}

void StateManager::showNotification(Data* data) {
    statesToInsert.emplace(new GameState(data, currRenderer->copy()->setMode(Renderer::NOTIFICATION), 
        new GenericInputHandler<Data>(data, [](Data* data, const std::string& choice){
        int number = std::stoi(choice);
        if (number == 1 || number == 0) { Game::getCurrentGame()->getManager()->pop(); return; }
        throw std::out_of_range(data->getTitle());
    })));
}

void StateManager::showError(Data* data) {
    push(data, currRenderer->copy()->setMode(Renderer::ERROR));
    render();
    pop();
}

bool StateManager::isEmpty() const { return stateStack.size() == 0; }

void StateManager::clear() {
    while(stateStack.size()) {
        statesToDelete.push(stateStack.top());
        stateStack.pop();
    }
}



