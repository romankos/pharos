#ifndef game_h
#define game_h

#include <fstream>
#include <string>

#include "character.h"
#include "stateManager.h"
#include "fileManager.h"
#include "serializable.h"

class StateManager;
class Character;
class FileManager;

/**
 * Class Game implements Serializable
 * Contains information abour current game
 */
class Game : public Serializable {
private:
    /** Holds the current game */
    static Game* currentGame;
    
    /** Holds the game's state manager */
    StateManager* gameManager;
    /** Holds the game's controlled character */
    Character* gameCharacter;
    /** Holds the game's file manager */
    FileManager* fileManager;


public:
    static Game* getCurrentGame();

    /**
     * Constructs an object of Game from stateManager and manager.
     * 
	 * @param[in] stateManager StateManager.
	 * @param[in] manager FileManager.
     */
    Game(StateManager* stateManager, FileManager* manager);
    ~Game();

    /**
     * Method start loads and starts a game from save, contains game loop.
     * 
	 * @param[in] save relative path to a save file. 
     */
    void start(const std::string& save);
    
    StateManager* getManager() const;
    FileManager* getFileManager() const;
    Character* getCharacter() const;
    void setCharacter(Character* character);

    bool fromFile(std::ifstream& file);

    bool toFile(std::ofstream& file) const override;
};

#endif