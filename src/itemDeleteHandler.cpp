#include "itemDeleteHandler.h"
#include "game.h"
#include "location.h"


ItemDeleteHandler::ItemDeleteHandler(Character* character, const std::list<std::string>& items) : character(character)  {
    int i = 1;
    for (auto item : items) {
        this->items[i++] = item;
    }
}

void ItemDeleteHandler::handle(const std::string& data) const {
    int number = std::stoi(data);
    if (number == 0) { Game::getCurrentGame()->getManager()->pop(); return; }
    character->deleteItem(items.at(number));
    Location::getLocationOf(character)->addItem(items.at(number));
    Game::getCurrentGame()->getManager()->pop();
}