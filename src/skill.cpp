#include <map>
#include <cmath>
#include <fstream>

#include "skill.h"
#include "genericData.h"

const std::string Skill::hp = "Energy";

std::map<std::string, Skill*> Skill::skills;
const int CharacterSkill::lvlMultiplier = 2;

void Skill::clear() { skills.clear(); }
bool Skill::fromFile(std::ifstream& file) {
    std::string id;
    std::string name;
    std::string description;
    std::string tmp;
    if(!file.good()) { return false; }
    while(tmp != "ID:" && !file.eof()) { std::getline(file, tmp); }
    while (!file.eof()) {
        id = name = description = tmp = "";
        std::getline(file, tmp);
        while (tmp != "Title:" && !file.eof()) {
            if (tmp != "") id += tmp + "\n";
            std::getline(file, tmp);
        }
        std::getline(file, tmp);
        while (tmp != "Description:" && !file.eof()) {
            if (tmp != "") name += tmp + "\n";
            std::getline(file, tmp);
        }
        std::getline(file, tmp);
        while (tmp != "ID:" && !file.eof()) {
            if (tmp != "") description += tmp + "\n";
            std::getline(file, tmp);
        }
        if( id != "") id.pop_back();
        if( name != "") name.pop_back();
        if( description != "") description.pop_back();
        new Skill(id, name, description);
        if (!file.good() && !file.eof()) { return false; }
    }
    return file.good() || file.eof();
}
//throws std::out_of_bound
Skill* Skill::getSkill(const std::string& id) { return skills.at(id); }

std::list<std::string> Skill::getSkills() {
    std::list<std::string> result;
    for (auto e : skills) { result.emplace_back(e.first); }
    return result;
}

//throws Identity::UniqueArgumentException
Skill::Skill(const std::string& title, const std::string& description) 
: Skill(title, title, description) {}
Skill::Skill(const std::string& id, const std::string& title, const std::string& description) : Identity(id), title(title), description(description) { 
    if(!skills.emplace(id, this).second) { throw Identity::UniqueArgumentException("Skill with an id" + getId() + " already exists!"); }
} 

bool Skill::toFile(std::ofstream& file) const {
    file << "ID:\n" + getId() + "\nTitle:\n" + getTitle() + "\nDescription:\n" + getDescription() + "\n";
    return file.good();
}

std::string Skill::getTitle() const { return title; }
std::string Skill::getDescription() const { return description; }
std::list<std::string> Skill::getOptions() const { return {}; }


CharacterSkill::LevelUpNotification::LevelUpNotification() : title("") {}
CharacterSkill::LevelUpNotification::LevelUpNotification(const std::string& title) : title(title) {}


std::string CharacterSkill::LevelUpNotification::getTitle() const { return title; }
std::string CharacterSkill::LevelUpNotification::getDescription() const { return {}; }
std::list<std::string> CharacterSkill::LevelUpNotification::getOptions() const { return { "Continue" }; }

//throws std::out_of_bound
CharacterSkill::CharacterSkill(const std::string& id, int maxLevel, int currLevel, int regenerationPerTurn, int levelPoints) : 
skill(id), maxLevel(maxLevel), currentLevel(currLevel), regenerationPerTurn(regenerationPerTurn), levelPoints(levelPoints), levelPointsNeeded(maxLevel * lvlMultiplier) {
    if(maxLevel < 0) throw new GenericData("Can't create skill with negative level!", "", {});
}
CharacterSkill::CharacterSkill(const std::string& id, int maxLevel, int regenerationPerTurn, int levelPoints) : 
CharacterSkill::CharacterSkill(id, maxLevel, maxLevel, regenerationPerTurn, levelPoints) {}

void CharacterSkill::changeMaxBy(int points) { 
    if (maxLevel + points < 0) {
        changeMaxBy(-maxLevel);
    } else {
        currentLevel = maxLevel += points; levelPointsNeeded = maxLevel * lvlMultiplier;
    }
}
void CharacterSkill::changeCurrBy(int points) { 
    if (currentLevel + points >= 0) { 
        if (currentLevel + points <= maxLevel) {
            currentLevel += points; levelPoints += std::abs(points); 
        } else {
            levelPoints += maxLevel - currentLevel;
            currentLevel = maxLevel; 
        }
    } else { levelPoints += currentLevel; currentLevel = 0; }
    checkLevelUp();  
}
void CharacterSkill::changeRegenerationBy(int points) { regenerationPerTurn += points; }
int CharacterSkill::getCurrentLevel() const { return currentLevel; }
int CharacterSkill::getNeededPoints() const { return levelPointsNeeded; }
int CharacterSkill::getPoints() const { return levelPoints; }
int CharacterSkill::getRegeneration() const { return regenerationPerTurn; }
int CharacterSkill::getMaxLevel() const { return maxLevel; }
void CharacterSkill::update() {
    currentLevel += regenerationPerTurn;
    if(currentLevel > maxLevel) { currentLevel = maxLevel; }
}

void CharacterSkill::checkLevelUp() { 
    if (levelPoints >= levelPointsNeeded) {
        levelPoints = 0;
        changeMaxBy(10);
        changeRegenerationBy(1);
        throw new LevelUpNotification("leveled up! Now max " + skill + " is " + std::to_string(maxLevel) + "\nTo next level: " + std::to_string(levelPointsNeeded));  
    }
}

std::string CharacterSkill::getTitle() const { 
    return Skill::getSkill(skill)->getTitle(); 
}
std::string CharacterSkill::getDescription() const { 
    return Skill::getSkill(skill)->getTitle() + "\n" + Skill::getSkill(skill)->getDescription() + "\nmax:" + std::to_string(getMaxLevel()) + " curr:" + std::to_string(getCurrentLevel()) + " regen:" + std::to_string(regenerationPerTurn) + "\nPoints to new level: " + std::to_string(levelPointsNeeded - levelPoints); 
}
std::list<std::string> CharacterSkill::getOptions() const { return {}; }

