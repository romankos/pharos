#ifndef location_h
#define location_h

#include <set>
#include <list>
#include <string>
#include <fstream>

#include "identity.h"
#include "data.h"
#include "serializable.h"
#include "character.h"
#include "genericInputHandler.h"

class Character;
/**
 * Class Location implements Identity, Data, Serializable
 * One of the main game objects which contains information about the location.
 */
class Location : public Identity, public Data, public Serializable {
private:
    static std::map<std::string, Location*> locations;

    std::string title;
    std::string description;

    std::set<std::string> connectedLocations;
    std::set<std::string> characters;
    std::set<std::string> items;

    /**
     * Method addEnemies returns the list of enemies of a controlled character in this location.
     * 
	 * @param[in,out] options List of enemies of main character.
     */
    void addEnemies(std::list<std::string>& options) const;
    void updateLocation();

public:
    static const std::string start;
    static void clear();

    static bool fromFile(std::ifstream& file);

    static std::list<std::string> getLocations();

    static Location* getLocation(const std::string& name);
    static void removeCharacterFromAll(Character* character);
    static Location* getLocationOf(Character* character);

    //throws Identity::UniqueArgumentException
    Location(const std::string& id, const std::string& description);
    //throws Identity::UniqueArgumentException
    Location(const std::string& id, const std::string& title, const std::string& description);

    bool connectWith(const std::string& id);

    const std::set<std::string>& getConnectedLocations();
    const std::set<std::string>& getCharacters();

    //throws std::out_of_bound
    Location* getConnectedLocation(const std::string& id) const;

    //throws std::out_of_bound
    Character* getCharacter(const std::string& id) const;

    bool addCharacter(const std::string& name);

    void removeCharacter(const std::string& name);

    void addItem(const std::string& item);

    void deleteItem(const std::string& item);

    void update() override;

    bool toFile(std::ofstream& file) const override;

    virtual std::string getTitle() const override;
    virtual std::string getDescription() const override;
    virtual std::list<std::string> getOptions() const override;

};

#endif