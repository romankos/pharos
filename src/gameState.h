#ifndef gameState_h
#define gameState_h

#include <string>
#include "data.h"
#include "renderer.h"
#include "inputHandler.h"


/**
 * Class GameState
 * Acts as one of possible game states.
 */
class GameState {
private:
    /** Data of a current state */
    Data* dataComponent;
    /** Renderer of a current state */
    Renderer* renderComponent;
    /** Input Handler of a current state */
    const InputHandler* inputComponent;

public:
    GameState(Data* data, Renderer* renderer, const InputHandler* input);
    virtual ~GameState();

    virtual void enter() const;
    virtual void render() const;
    virtual void update();
    virtual void handleInput(std::string data) const;
    virtual void error(Data* data) const;
    virtual void exit() const;
};

#endif