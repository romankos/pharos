#ifndef consoleProvider_h
#define consoleProvider_h

#include <string>

#include "inputProvider.h"

/**
 * Class ConsoleProvider implements InputProvider.
 * Reads input from console.
 */
class ConsoleProvider : public InputProvider {
public:
    virtual ~ConsoleProvider();
    virtual std::string getInput() const override;
};

#endif