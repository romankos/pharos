#ifndef inputProvider_h
#define inputProvider_h

#include <string>

/**
 * Class InputProvider provides interface for input providers.
 */
class InputProvider {
public:
    virtual ~InputProvider() {}

   /**
	 * Method getInput reads input and provides it as a string
	 * 
     * @return Input as a string.
	 */
    virtual std::string getInput() const = 0;
};

#endif