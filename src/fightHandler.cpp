#include "fightHandler.h"
#include "location.h"
#include "genericInputHandler.h"
#include "genericData.h"
#include "itemHandler.h"

FightHandler::FightHandler(Fight* fight) : fight(fight) {}
FightHandler::~FightHandler() {}

void FightHandler::handle(const std::string& data) const {
    int number = std::stoi(data);
    if(number == 0) { Game::getCurrentGame()->getManager()->pop(); return; }
    
    fight->useEffectAt(number);
    if(fightWon()) { return; }
    fight->nextPhase();
    
    uncontrolledMoves();
}

void FightHandler::uncontrolledMoves() const {
    while (!fight->getAttacker()->isControlled()) {
        fight->makeMove();
        if(fightWon()) { return; }
        fight->nextPhase();
    }
}

bool FightHandler::fightWon() const {
    if(fight->getWinner() != NULL) {
        if(fight->getAttacker()->isControlled()) {
            Game::getCurrentGame()->getManager()->pop();
            Game::getCurrentGame()->getManager()->push(new GenericData(fight->getWinner()->getTitle() + " is victorious!", "Choose the fate of your enemy:", {"Execute", "Show mercy"}),
                new GenericInputHandler<Character>(fight->getTarget()->getCharacter(), [](Character* character, const std::string& choice){
                    int number = std::stoi(choice);
                    if (number == 0) { Game::getCurrentGame()->getManager()->pop(); return; } 
                    if (number == 1) {
                        Location::removeCharacterFromAll(character);
                        auto items = character->getItems();
                        if(items.size() > 0) {
                            Game::getCurrentGame()->getManager()->pop();
                            Game::getCurrentGame()->getManager()->push(new GenericData("Pick one item!", {}, {items}), new ItemHandler(Game::getCurrentGame()->getCharacter(), items));
                        } else {
                            Game::getCurrentGame()->getManager()->pop();
                        }
                        return; 
                    } 
                    if (number == 2) {
                        character->setAttitude(Game::getCurrentGame()->getCharacter()->getId(), AFRAID);
                        Game::getCurrentGame()->getCharacter()->setAttitude(character->getId(), UNFRIENDLY);
                        Game::getCurrentGame()->getManager()->pop();
                        return;
                    }
                    throw std::out_of_range("Choose one of the presented optoins!");
            }));

        } else if (fight->getTarget()->isControlled()) {
            Game::getCurrentGame()->getManager()->pop();
            Game::getCurrentGame()->getManager()->pop();
            Game::getCurrentGame()->getManager()->showInfo(new GenericData("You have lost!", "If only you had another chance...", {}));
        } else { }
        return true;
    } else { return false; }
}