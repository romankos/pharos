#include <iostream>
#include <string>
#include <sstream>

#include "consoleRenderer.h"
#include "location.h"
#include "character.h"
#include "item.h"
#include "game.h"
#include "effect.h"

void ConsoleRenderer::render(const Data* data) const {
    std::string flag;
    switch(getMode()) {
        case NORMAL : flag = "\033[37m"; break; 
        case NOTIFICATION : flag = "\033[36m"; break;
        case ERROR : flag = "\033[31m"; break;
    }
    for (int i = 0; i < 2; i++) { std::cout << std::endl; }

    if(data->getTitle() != "") { std::cout << flag << data->getTitle() << "\033[0m" << std::endl; }

    if(data->getDescription() != "") { std::cout << flag << data->getDescription() << "\033[0m" << std::endl; }
    
    int i = 1;
    for(std::string option : data->getOptions()) {
        std::stringstream ss(option);
        std::string to;
        std::getline(ss, to, '\n');
        std::cout << "\033[36m\t[" << i++ << "] " << "\033[0m" <<  getPrefix(to) << std::endl;
        while(std::getline(ss, to, '\n')) {
            std::cout << '\t' << to << std::endl;
        }
    }
}

std::string ConsoleRenderer::getPrefix(const std::string& str) const {
    try { Effect::getEffect(str); return "" + Effect::getEffect(str)->getTitle(); } catch (const std::out_of_range& e) {}
    try { Location::getLocation(str); return "Go to " + Location::getLocation(str)->getTitle(); } catch (const std::out_of_range& e) {}
    try { 
        switch (Character::getCharacter(str)->getAttitude(Game::getCurrentGame()->getCharacter())) {
            case ENEMY      : return "Fight " + Character::getCharacter(str)->getTitle();
            case YOURSELF   : return "Check info: " + Character::getCharacter(str)->getTitle();
            default         : return "Talk to " + Character::getCharacter(str)->getTitle();    
        }
    } catch (const std::out_of_range& e) {}
    try { Item::getItem(str); return "Take " + Item::getItem(str)->getTitle(); } catch (const std::out_of_range& e) {}
    try { Skill::getSkill(str); return "Choose " + Skill::getSkill(str)->getTitle(); } catch (const std::out_of_range& e) {}
    return "" + str;
}

ConsoleRenderer* ConsoleRenderer::copy() const { return new ConsoleRenderer(*this); }