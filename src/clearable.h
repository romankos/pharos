#ifndef clearable_h
#define clearable_h

#include <string>
#include <unordered_set>

/**
 * Class Clearable contains and clears all of its objects.
 */
class Clearable {
private:
    static std::unordered_set<Clearable*> data;

public:
    static void clear();

    Clearable(); 
    virtual ~Clearable() {}
};

#endif