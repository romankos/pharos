#include <map>
#include <list>
#include <string>

#include "location.h"
#include "game.h"

const std::string Location::start = "start";


std::map<std::string, Location*> Location::locations = {};

void Location::clear() { locations.clear(); }

bool Location::fromFile(std::ifstream& file) {
    std::string id;
    std::string title;
    std::string description;
    std::string tmp;
    if(!file.good()) { return false; }
    while(tmp != "ID:" && !file.eof()) { std::getline(file, tmp); }
    while (!file.eof()) {
        id = title = description = tmp = "";
        std::getline(file, tmp);
        while (tmp != "Title:" && !file.eof()) {
            if (tmp != "") id += tmp + "\n";
            std::getline(file, tmp);
        }
        std::getline(file, tmp);
            while (tmp != "Description:" && !file.eof()) {
            if (tmp != "") title += tmp + "\n";
            std::getline(file, tmp);
        }
        std::getline(file, tmp);
            while (tmp != "Locations:" && !file.eof()) {
            if (tmp != "") description += tmp + "\n";
            std::getline(file, tmp);
        }
        if (id != "") id.pop_back();
        if (title != "") title.pop_back();
        if (description != "") description.pop_back();
        Location* newChar = new Location(id, title, description);
        std::getline(file, tmp);
        while (tmp != "Characters:" && !file.eof()) {
            if (tmp != "") newChar->connectWith(tmp);
            std::getline(file, tmp);
        }
        std::getline(file, tmp);
        while (tmp != "Items:" && !file.eof()) {
            if (tmp != "") newChar->addCharacter(tmp);
            std::getline(file, tmp);
        }
        std::getline(file, tmp);
        while (tmp != "ID:" && !file.eof()) {
            if (tmp != "") newChar->addItem(tmp);
            std::getline(file, tmp);
        }
        if (!file.good() && !file.eof()) { return false; }
    }
    return file.good() || file.eof();
}

bool Location::toFile(std::ofstream& file) const {
    file << "ID:\n" + getId() + "\nTitle:\n" + getTitle() + "\nDescription:\n" + getDescription() + "\nLocations:\n";
    for (auto e : connectedLocations) { file << e << "\n"; }
    file << "Characters:\n";
    for (auto e : characters) { file << e << "\n"; }
    file << "Items:\n";
    for (auto e : items) { file << e << "\n"; }
    return file.good();
}

std::list<std::string> Location::getLocations() {
    std::list<std::string> result;
    for(auto e : locations) { result.emplace_back(e.first); }
    return result;
}

Location* Location::getLocation(const std::string& name) { 
    return locations.at(name);
}

void Location::removeCharacterFromAll(Character* character) {
    for (auto l : locations) {
        l.second->removeCharacter(character->getId());
    }
}

Location* Location::getLocationOf(Character* character) { 
    for (auto l : locations) {
        if (l.second->characters.count(character->getId())) { return l.second; }
    }
    throw std::out_of_range("Character could not be located");
}


Location::Location(const std::string& id, const std::string& description) 
    : Location::Location(id, id, description) {}

Location::Location(const std::string& id, const std::string& title, const std::string& description)
    : Identity(id), title(title), description(description) { 
    if(!locations.emplace(id, this).second) { throw Identity::UniqueArgumentException("Location with an id" + getId() + " already exists!"); } 
}


void Location::addEnemies(std::list<std::string>& options) const {
    for (auto character : characters) { if (Character::getCharacter(character)->getAttitude(Game::getCurrentGame()->getCharacter()) == Attitude::ENEMY) {options.emplace_back(character); }}
}

bool Location::connectWith(const std::string& id) { 
    return connectedLocations.emplace(id).second;
}

const std::set<std::string>& Location::getConnectedLocations() { 
    return connectedLocations; 
}

const std::set<std::string>& Location::getCharacters() { 
    return characters; 
}

Location* Location::getConnectedLocation(const std::string& id) const {
    if(connectedLocations.count(id)) { return Location::getLocation(id); }
    throw std::out_of_range("");
}

Character* Location::getCharacter(const std::string& id) const {
    if(characters.count(id)) { return Character::getCharacter(id); }
    throw std::out_of_range("");
}

bool Location::addCharacter(const std::string& name) {
    return characters.emplace(name).second; 
}

void Location::removeCharacter(const std::string& name) {
    characters.erase(name);
}

void Location::addItem(const std::string& item) {
    items.emplace(item);
}

void Location::deleteItem(const std::string& item) {
    items.erase(item);
}

void Location::updateLocation() {
    for (auto e : characters) { Character::getCharacter(e)->update(); }
}

void Location::update() {
    for(auto e : locations) { e.second->updateLocation(); }
}

std::string Location::getTitle() const { return title; }
std::string  Location::getDescription() const { return description; }
std::list<std::string> Location::getOptions() const {
    std::list<std::string> options;
    addEnemies(options);
    if(options.size() != 0) {
        options.emplace_back("Save");
        options.emplace_back("Exit");
        return options;
    }
    for (auto e : connectedLocations) { options.emplace_back(e); }
    for (auto e : items) { options.emplace_back(e); }
    for (auto e : characters) { options.emplace_back(e); }
    options.emplace_back("Save");
    options.emplace_back("Exit");
    return options;
}


