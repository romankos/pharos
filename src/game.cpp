#include <list>

#include "game.h"
#include "characterCreator.h"
#include "locationHandler.h"
#include "playerHandler.h"
#include "consoleProvider.h"
#include "consoleRenderer.h"
#include "genericData.h"

Game* Game::currentGame = NULL;

Game* Game::getCurrentGame() { return currentGame; }

Game::Game(StateManager* manager, FileManager* fileManager) : gameManager(manager), gameCharacter(NULL), fileManager(fileManager) {
    currentGame = this;
}


Game::~Game() { delete fileManager; delete gameManager; }

StateManager* Game::getManager() const { return gameManager; }
FileManager* Game::getFileManager() const { return fileManager; }
Character* Game::getCharacter() const { return gameCharacter; }
void Game::setCharacter(Character* character) { gameCharacter = character; }


void Game::start(const std::string& save) {
    
    getManager()->clear();
    if(!getFileManager()->loadGame(save)) {
        throw new GenericData("Error loading game!", "", {});
    }

    Data* menuData = new GenericData("Main Menu", "", { "New Game", "Load Game", "Exit" } );

    GenericInputHandler<Data>* menuHandler = new GenericInputHandler<Data>(menuData, [](Data* data, const std::string& choice) {

        int number = std::stoi(choice);
        int i = 1;
        std::map<int, std::string> options; for (auto e : data->getOptions()) {options.emplace(i++, e); }
        if(options.at(number) == "Load Game") {
            FileManager* fileManager = new FileManager();
            GenericData* loadMenu = new GenericData("Load game:", "", fileManager->getSaves());
            delete fileManager;
            Game::getCurrentGame()->getManager()->push(loadMenu, new GenericInputHandler<GenericData>(loadMenu,
                    [](GenericData* data, const std::string& choice) {
                        int number = std::stoi(choice);
                        auto options = data->getOptions();
                        if((int)options.size() < number--) { throw std::out_of_range(""); }
                        auto option = *std::next(options.begin(), number);
                        if(option == "Exit") {
                            Game::getCurrentGame()->getManager()->pop(); return;
                        } else {
                            getCurrentGame()->start(option);
                        }
                    }));
            return;
        } else if (options.at(number) == "New Game") {
            CharacterCreator* characterCreator = new CharacterCreator({{0, "Input name:"}, {1, "Input number of skill and desired number of points:"}, {2, "Pick one item:"}}, {{0, []() -> std::list<std::string> { return {}; }}, {1, Skill::getSkills}, {2, Item::getItems}});
            getCurrentGame()->getManager()->push(characterCreator, new PlayerHandler(characterCreator));
            return;
        } else if (options.at(number) == "Exit") {
            getCurrentGame()->getManager()->pop();
            return;
        }
        throw std::out_of_range("");
    });

    getManager()->push(menuData, menuHandler);


    if(gameCharacter != NULL) {
        getManager()->push(Location::getLocationOf(gameCharacter), new LocationHandler(Location::getLocationOf(gameCharacter)));
    }

    while (!gameManager->isEmpty()) {
        gameManager->update();
    }
}

bool Game::fromFile(std::ifstream& file) {
    std::string id;
    std::string tmp;
    if(!file.good() && !file.eof()) { return false; }
    while(tmp != "Game Character:" && !file.eof()) { std::getline(file, tmp); }
    while (!file.eof()) {
        std::getline(file, tmp);
        if (tmp != "") id += tmp + "\n";
    }
    if(id != "") id.pop_back();
    if(id == "NULL") { gameCharacter = NULL; }
    else { gameCharacter = Character::getCharacter(id); }
    return file.good() || file.eof();
}

bool Game::toFile(std::ofstream& file) const {
    file << "Game Character:\n" << (gameCharacter == NULL ? "NULL" : gameCharacter->getId());
    return file.good();
}