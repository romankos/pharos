#include <list>

#include "locationHandler.h"
#include "fightHandler.h"
#include "genericData.h"
#include "dialogHandler.h"
#include "itemDeleteHandler.h"
#include "character.h"
#include "fight.h"
#include "game.h"


LocationHandler::LocationHandler(Location* location) : location(location) {}
LocationHandler::~LocationHandler() {}

void LocationHandler::handle(const std::string& data) const {
    int number = std::stoi(data);
    if(number == 0) { Game::getCurrentGame()->getManager()->pop(); return; }

    auto options = location->getOptions();
    if((int)options.size() < number--) { throw std::out_of_range(""); }
    auto option = *std::next(options.begin(), number);
    if(option == "Save") {
        Data* saveMenu = new GenericData("Input save name:", "", {});
        Game::getCurrentGame()->getManager()->push(saveMenu, new GenericInputHandler<Data>(saveMenu, 
            [](Data* data, const std::string& choice) {
                if(Game::getCurrentGame()->getFileManager()->saveGame(choice)) {
                    Game::getCurrentGame()->getManager()->pop();
                } else {
                    throw new GenericData(data->getTitle(), "Error saving game", {});
                }
            }));
        return;
    } else if (option == "Exit") {
        Game::getCurrentGame()->start(FileManager::src);
        return;
    }
    try {
        Location* loc = Location::getLocation(option);
        location->removeCharacter(Game::getCurrentGame()->getCharacter()->getId());
        loc->addCharacter(Game::getCurrentGame()->getCharacter()->getId());
        loc->update();
        Game::getCurrentGame()->getManager()->pop();
        Game::getCurrentGame()->getManager()->push(loc, new LocationHandler(loc));
        return;
    } catch (const std::out_of_range& e) {}
    try {         
        Character* character = Character::getCharacter(option);
        if(character->getAttitude(Game::getCurrentGame()->getCharacter()) == ENEMY) {
            Fight* fight = new Fight(new Army(Game::getCurrentGame()->getCharacter(), true), new Army(character));
            Game::getCurrentGame()->getManager()->push(fight, new FightHandler(fight));
        } else {
            std::list<std::string> dialogOptions = {"Tell me about yourself"};
            auto effects = Effect::getEffects(Game::getCurrentGame()->getCharacter(), character);
            for (auto e : effects) { dialogOptions.emplace_back(e->getId()); }
            Game::getCurrentGame()->getManager()->push(new GenericData(character->getTitle(), character->getGreeting(), dialogOptions), new DialogHandler(Game::getCurrentGame()->getCharacter(), character, dialogOptions));
        }
        return;
    } catch (const std::out_of_range& e) {}        
    try {         
        Item::getItem(option);
        if (!Game::getCurrentGame()->getCharacter()->addItem(option)) {
            Game::getCurrentGame()->getManager()->push(new GenericData("Choose an item to drop", "", Game::getCurrentGame()->getCharacter()->getItems()), new ItemDeleteHandler(Game::getCurrentGame()->getCharacter(), Game::getCurrentGame()->getCharacter()->getItems()));
        } else {
            location->deleteItem(option);
        }
        return;
    } catch (const std::out_of_range& e) {}        
    
    throw std::out_of_range("");
}