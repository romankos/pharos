#ifndef item_h
#define item_h

#include <string>
#include <map>

#include "data.h"
#include "identity.h"
#include "serializable.h"

/**
 * Class Item implements Identity, Data, Serializable
 * One of the main game objects which contains information about an item.
 */
class Item : public Data, public Identity, public Serializable {
private:
    static std::map<std::string, Item*> items;
    std::string title;
    std::string description;

public:
    static void clear();
    static bool fromFile(std::ifstream& file);

    //throws std::out_of_bound
    static Item* getItem(const std::string& id);
    
    static std::list<std::string> getItems();

    //throws Identity::UniqueArgumentException
    Item(const std::string& id, const std::string& description);
    Item(const std::string& id, const std::string& title, const std::string& description);

    bool toFile(std::ofstream& file) const;

    virtual std::string getTitle() const override;
    virtual std::string getDescription() const override;
    virtual std::list<std::string> getOptions() const override;
};


#endif