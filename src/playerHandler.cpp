#include <list>
#include <iterator>
#include <sstream>

#include "playerHandler.h"
#include "genericData.h"
#include "skill.h"
#include "item.h"
#include "characterCreator.h"
#include "locationHandler.h"
#include "game.h"



PlayerHandler::PlayerHandler(CharacterCreator* character) : character(character) {}

void PlayerHandler::handle(const std::string& data) const {
    if(data == "0") { Game::getCurrentGame()->getManager()->pop(); return; }

    switch(character->getPhase()) {
    case 0 :
        character->create(data);
        character->update();
        break;
    case 1 :
        if (character->getSkillPoints()) {
            std::istringstream iss (data);
            std::vector<std::string> tokens{std::istream_iterator<std::string> {iss}, std::istream_iterator<std::string>{}};
            if(tokens.size() != 2) {
                throw new GenericData("Input fromat: 'skill number' 'points'", "", {});
            }
            int points = std::stoi(tokens.at(1));
            if(points > character->getSkillPoints()) {
                throw new GenericData("You don't have enough points", "", {});
            }
            std::string name = *std::next(character->getOptions().begin(), std::stoi(tokens.at(0)) - 1);
            bool hasSkill = false;
            try { 
                character->getCharacter()->getSkill(name)->changeMaxBy(points);
                hasSkill = true;
            } catch (const std::out_of_range& e) {  }

            if(!hasSkill) { character->getCharacter()->addSkill(new CharacterSkill(name, points)); }    

            character->deduceSkillPoints(std::stoi(tokens.at(1)));
        }
        if (!character->getSkillPoints()) { character->update(); }
        break;
    case 2 :
        character->getCharacter()->addItem(*std::next(character->getOptions().begin(), std::stoi(data) - 1));
        character->update();
        break;
    default:
        break;
    }

    if(character->isFinished()) {
        Game::getCurrentGame()->setCharacter(character->getCharacter());
        Game::getCurrentGame()->getManager()->showNotification(new GenericData("Your character is created!\n" + character->getCharacter()->getTitle(), character->getCharacter()->getDescription(), {"Continue"}));
        try { Game::getCurrentGame()->getCharacter()->getSkill(Skill::hp); } catch (const std::out_of_range& e) { 
            Game::getCurrentGame()->getCharacter()->addSkill(new CharacterSkill(Skill::hp, 0));
        }
        Game::getCurrentGame()->getManager()->pop();
        Location* start = Location::getLocation(Location::start);
        start->addCharacter(character->getCharacter()->getId());
        Game::getCurrentGame()->getManager()->push(start, new LocationHandler(start));
    }
}
