#ifndef effect_h
#define effect_h

#include <string>
#include <map>
#include <list>

#include "data.h"
#include "characterManipulator.h"
#include "serializable.h"

class Game;
class CharacterManipulator;
class Character;

/**
 * Class Effect implements Identity, Data, Serializable
 * One of the main game objects which contains information about the effect.
 * Can be cast by one character (user) upon another character (target).
 */
class Effect : public Data, public Identity, public Serializable {
private:
    static std::map<std::string, Effect*> effects;

    std::string title;

    /** Condition needed for effect to be used. */
    CharacterManipulator* condition;
    /** Manipulation appied to target. */
    CharacterManipulator* effectOnTarget;
    /** Manipulation appied to user. */
    CharacterManipulator* effectOnUser;

public:
    static void clear();

    static bool fromFile(std::ifstream& file);
    
    static std::list<std::string> getEffects();

    /**
     * Method getEffects provides a list of effects' ids, which can be cast by a user upon himself.
     * 
     * @param[in] user User casting the effect.
     * @return List of possible effects.
     */
    static std::list<Effect*> getEffects(Character* user);

    /**
     * Method getEffects provides a list of effects' ids, which can be cast by a user upon a target.
     * 
     * @param[in] user User casting the effect.
     * @param[in] target Target of the effect.
     * @return List of possible effects.
     */
    static std::list<Effect*> getEffects(Character* user, Character* target);

    static Effect* getEffect(const std::string& id);

    Effect (const std::string& title, CharacterManipulator* condition, CharacterManipulator* effectOnTarget, CharacterManipulator* effectOnUser);

    Effect (const std::string& id, const std::string& title, CharacterManipulator* condition, CharacterManipulator* effectOnTarget, CharacterManipulator* effectOnUser);

    bool canBeUsed(Character* user, Character* target);

    void use(Character* user, Character* target);

     bool toFile(std::ofstream& file) const;

    virtual std::string getTitle() const;
    virtual std::string getDescription() const;
    virtual std::list<std::string> getOptions() const override;
};

#endif