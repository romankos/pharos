#ifndef itemDeleteHandler_h
#define itemDeleteHandler_h

#include <map>
#include <list>

#include "inputHandler.h"
#include "character.h"


class Game;

/**
 * Class ItemDeleteHandler implements InputHandler.
 * Handles input in the context of Item Delete Menu.
 */
class ItemDeleteHandler : public InputHandler {
private:
    Character* character;
    std::map<int, std::string> items;

public:
    /**
     * Constructs an object of ItemDeleteHandler from character and his items.
     * 
	 * @param[in] character Character to delete an item. 
	 * @param[in] items List of items to delete. 
     */
    ItemDeleteHandler(Character* character, const std::list<std::string>& items);

    virtual void handle(const std::string& data) const override;
};

#endif