#include "itemHandler.h"
#include "itemDeleteHandler.h"
#include "genericData.h"


ItemHandler::ItemHandler(Character* character, const std::list<std::string>& items) : character(character)  {
    int i = 1;
    for (auto item : items) {
        this->items[i++] = item;
    }
}

void ItemHandler::handle(const std::string& data) const {
    int number = std::stoi(data);
    if (number == 0) { Game::getCurrentGame()->getManager()->pop(); return; }
    if (!character->addItem(items.at(number))) {
        Game::getCurrentGame()->getManager()->push(new GenericData("Choose an item to drop", "", character->getItems()), new ItemDeleteHandler(character, character->getItems()));
        return;
    }
    Game::getCurrentGame()->getManager()->pop();
}