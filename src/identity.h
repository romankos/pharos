#ifndef identity_h
#define identity_h

#include <string>
#include <set>

#include "clearable.h"
#include "data.h"

/**
 * Class Identity provides an id mechanism.
 */
class Identity {
private:
    const std::string id;

public:

    /**
     * Class UniqueArgumentException implements Data and acts as an exception.
     */
    class UniqueArgumentException : public Data {
    private:
        const std::string message; 
    public:
        UniqueArgumentException(const std::string& message) : message(message) {}
        virtual std::string getTitle() const override { return "UniqueArgumentException"; }
        virtual std::string getDescription() const { return message; }
        virtual std::list<std::string> getOptions() const { return {"Continue", "Exit"}; }
    };

    Identity(const std::string& id) : id(id) {}

    virtual ~Identity() {}

    const std::string& getId() const { return id; }
};

#endif