#ifndef fileManager_h
#define fileManager_h

#include <string>
#include <list>


/**
 * Class FileManager
 * Loads and saves the Games
 */
class FileManager {
private:
    std::list<std::string> saves;

public:
    static const std::string src;
    static const std::string path;
 

    bool loadGame(const std::string& save);
    bool saveGame(const std::string& save);


    std::list<std::string>& getSaves();
};

#endif