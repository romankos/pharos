#include <set>
#include <fstream>
#include <iostream>
#include "item.h"

std::map<std::string, Item*> Item::items = {};

void Item::clear() { items.clear(); }

Item* Item::getItem(const std::string& id) { 
    return items.at(id); 
}

bool Item::fromFile(std::ifstream& file) {
    std::string id;
    std::string name;
    std::string description;
    std::string tmp;
    if(!file.good() && !file.eof()) { return false; }
    while(tmp != "ID:") { std::getline(file, tmp); }
    while (!file.eof()) {
        id = name = description = tmp = "";
        std::getline(file, tmp);
        while (tmp != "Title:" && !file.eof()) {
            if (tmp != "") id += tmp + "\n";
            std::getline(file, tmp);
        }
        std::getline(file, tmp);
        while (tmp != "Description:" && !file.eof()) {
            if (tmp != "") name += tmp + "\n";
            std::getline(file, tmp);
        }
        std::getline(file, tmp);
            while (tmp != "ID:" && !file.eof()) {
            if (tmp != "") description += tmp + "\n";
            std::getline(file, tmp);
        }
        if(id != "") id.pop_back();
        if(name != "") name.pop_back();
        if(description != "") description.pop_back();
        new Item(id, name, description);
        if (!file.good() && !file.eof()) { return false; }
    }
    return file.good() || file.eof();
}

std::list<std::string> Item::getItems() {
    std::list<std::string> result;
    for (auto e : items) { result.emplace_back(e.first); }
    return result;
}

//throws Identity::UniqueArgumentException
Item::Item(const std::string& id, const std::string& description) : Item (id, id, description) {}
Item::Item(const std::string& id, const std::string& title, const std::string& description) : Identity(id), title(title), description(description) { 
    if (!items.emplace(id, this).second) { throw Identity::UniqueArgumentException("Item with an id: " + getId() + " already exists"); }
}

bool Item::toFile(std::ofstream& file) const {
    file << "ID:\n" + getId() + "\nTitle:\n" + getTitle() + "\nDescription:\n" + getDescription() + "\n";
    return file.good();
}

std::string Item::getTitle() const { return title; }
std::string Item::getDescription() const { return description; }
std::list<std::string> Item::getOptions() const { return {}; }
