#include <iostream>

#include "consoleProvider.h"

ConsoleProvider::~ConsoleProvider() {}
std::string ConsoleProvider::getInput() const {
    std::string result;
    std::cout << std::endl << "Your input: " << "\033[36m";
    std::getline(std::cin, result);
    std::cout << "\033[0m";
    return result;
}
