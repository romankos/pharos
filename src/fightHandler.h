#ifndef fightHandler_h
#define fightHandler_h

#include <list>

#include "game.h"
#include "inputHandler.h"
#include "fight.h"

class Game;

/**
 * Class FightHandler implements InputHandler.
 * Handles input in the context of Fight.
 */
class FightHandler : public InputHandler {
private:
    Fight* fight;

public:
    FightHandler(Fight* fight);
    virtual ~FightHandler();

    virtual void handle(const std::string& data) const override;

    /**
     * Method uncontrolled moves lets the Fight object process moves, not controlled by a player.
     *
     */
    void uncontrolledMoves() const;

    /**
     * Method fightWon checks, if Fight has been won, and commits further nessecary actions.
     *
     * @return true if Fight has been won, false - otherwise.
     */
    bool fightWon() const;
};

#endif