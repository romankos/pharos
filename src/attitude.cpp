#include <map>

#include "attitude.h"

std::map<Attitude, std::string> attitudeMap = {{DEAD, "Dead"}, {ENEMY, "Enemy"}, {AFRAID, "Afraid"}, {UNFRIENDLY, "Unfriendly"},
                                        {NEUTRAL, "Neutral"}, {FRIENDLY, "Friendly"}, {ALLY, "Ally"}, {YOURSELF, "Yourself"}};

std::string attitudeToString(Attitude a) {
    return attitudeMap.at(a);
}

Attitude attitudeFromString(std::string src) {
    for (auto e : attitudeMap) { if(e.second == src) { return e.first; } }
    return NEUTRAL;
}