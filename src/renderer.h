#ifndef renderer_h
#define renderer_h

#include "data.h"

/**
 * Class Renderer provides interface for rendering Data.  
 */

class Renderer {
public:
    /** Render MODE */
    enum MODE { NORMAL, ERROR, NOTIFICATION};
private:
    MODE mode;
public:
    virtual ~Renderer();

    /**
	 * Method render renders the Data.
	 * 
	 * @param[in] data Pointer to the Data object.
	 */
    virtual void render(const Data* data) const = 0;

    /**
	 * Method setMode sets the render MODE
	 * 
	 * @param[in] mode Render Mode.
     * @return Pointer to the current renderer.
	 */
    Renderer* setMode(MODE mode);

    /**
	 * Method copy creates a copy of the current renderer.
	 * 
	 * @param[in] mode Render Mode.
     * @return Pointer to the created renderer.
	 */
    virtual Renderer* copy() const = 0;

    /**
	 * Method getMode return render MODE of current renderer. 
	 * 
     * @return Render MODE.
	 */
    MODE getMode() const;
};

#endif