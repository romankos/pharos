#ifndef consoleRenderer_h
#define consoleRenderer_h

#include <string>

#include "renderer.h"

/**
 * Class ConsoleRenderer implements interface Renderer.
 * Renders the Data to console.
 */
class ConsoleRenderer : public Renderer {
public:
    virtual void render(const Data* data) const override;

    /**
	 * Method getPrefix adds the prefix to the given string
	 * 
	 * @param[in] str Given string.
     * @return String str with added prefix.
	 */
    std::string getPrefix(const std::string& str) const;

    ConsoleRenderer* copy() const override;

};

#endif