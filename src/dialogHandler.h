#ifndef dialogHandler_h
#define dialogHandler_h

#include <map>
#include <list>

#include "inputHandler.h"
#include "character.h"
#include "game.h"

class Game;

/**
 * Class DialogHandler implements InputHandler.
 * Handles input in the context of Dialog Menu.
 */
class DialogHandler : public InputHandler {
private:
    Character* talking;
    Character* talkedTo;
    std::map<int, std::string> options;

public:
    DialogHandler(Character* talking, Character* talkedTo, const std::list<std::string>& options);

    virtual void handle(const std::string& data) const override;
};

#endif