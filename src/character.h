#ifndef character_h
#define character_h

#include <map>
#include <set>
#include <list>

#include "identity.h"
#include "data.h"
#include "skill.h"
#include "attitude.h"
#include "item.h"
#include "serializable.h"

/**
 * Class Character implements Identity, Data, Serializable
 * One of the main game objects which contains information about the character.
 */
class Character : public Identity, public Data, public Serializable {
private:
    /** Maximum number of items in inventory */
    static const int itemCount;
    /** Map of all the characters */
    static std::map<std::string, Character*> characters;

    std::string title;
    std::string greeting;

    /** Map of skills of a character*/
    std::map<std::string, CharacterSkill*> skills;
    /** Map of attitudes of a character*/
    std::map<std::string, Attitude> attitudes;
    /** Map of items of a character*/
    std::set<std::string> items;

public:
    /**
     * Method clean cleans the map of all the characters. 
     */
    static void clear();
    
    /**
     * Method fromFile reads characters from file.
     * 
	 * @param[in] file Input File Stream to read characters from.
	 * @return Flag indicating the success of reading characters from file.
     */
    static bool fromFile(std::ifstream& file);

    /**
     * Method getCharacter finds Character by id.
     * 
	 * @param[in] id Character id.
	 * @return Pointer to the found character.
     * @throws std::out_of_range if character wasn't found.
     */
    static Character* getCharacter(const std::string& id);

    /**
     * Method getCharacters returns the list of all characters' ids.
     * 
	 * @return List of all characters' ids.
     */
    static std::list<std::string> getCharacters();


    /**
     * Constructs a character from title and greeting.
     * 
	 * @param[in] title as both title and id.
	 * @param[in] greeting.
     * @throws UniqueArgumentException if character with such an id already exists.
     */
    Character(const std::string& title, const std::string& greeting);
    
    /**
     * Constructs a character from id, title and greeting.
     * 
	 * @param[in] id.
	 * @param[in] title.
	 * @param[in] greeting.
     * @throws UniqueArgumentException if character with such an id already exists.
     */
    Character(const std::string& id, const std::string& title, const std::string& greeting);

    bool addSkill(CharacterSkill* skill);
    
    CharacterSkill* getSkill(const std::string& name) const;
    
    bool addItem(const std::string& name);

    bool hasItem(const std::string& name) const;

    void deleteItem(const std::string& name);

    std::list<std::string> getItems() const;

    void setAttitude(const std::string& id, Attitude attitude);

    Attitude getAttitude(Character* character) const;

    /**
     * Method update updates characters state.
     */
    void update();

    std::string getGreeting() const;

    /**
     * Method toFile erializes a character to file.
     * 
	 * @param[in] file Output File Stream to which a character should be written.
	 * 
	 * @return Flag indicating the success of writing characters to file.
     */
    bool toFile(std::ofstream& file) const override;

    virtual std::string getTitle() const override;
    virtual std::string getDescription() const override;
    virtual std::list<std::string> getOptions() const override;

    std::string shortDiscription() const;
};


#endif