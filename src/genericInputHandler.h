#ifndef genericInputHandler_h
#define genericInputHandler_h

#include "data.h"
#include "inputHandler.h"

/**
 * Class GenericInputHandler implements InputHandler.
 * Handles input in the context, provided by generic type.
 */
template <typename T>
class GenericInputHandler : public InputHandler {
private:
    /** Function to handle the input in the context of generic type. */ 
    typedef void (*DataProcessorFunction)(T*, const std::string&);
    T* data;
    DataProcessorFunction function;

public:
    GenericInputHandler(T* data, DataProcessorFunction function) : data(data), function(function) {}
    virtual void handle(const std::string& data) const { function(this->data, data); }
};

#endif