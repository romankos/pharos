#include "characterCreator.h"

CharacterCreator::CharacterCreator(const std::map<int, std::string>& phaseText, const std::map<int, function>& phaseActions) 
: character(NULL), phaseText(phaseText), phaseActions(phaseActions), phase(0), finalPhase(phaseActions.size()), skillPoints(300) {}

void CharacterCreator::create(const std::string& name) {
    character = new Character("main_character", name, "Your character");
}

void CharacterCreator::update() { phase++; }
int CharacterCreator::getPhase() { return phase; }
int CharacterCreator::getSkillPoints() { return skillPoints; }
int CharacterCreator::deduceSkillPoints(int points) { 
    if (skillPoints - points < 0) { 
        points = skillPoints; skillPoints = 0; return points; 
    } else if (skillPoints - points > 300) { 
        points = skillPoints - 300; skillPoints = 300; return points; 
    } else {
        skillPoints -= points; return points; 
    }
}
bool CharacterCreator::isFinished() { return phase >= finalPhase; }
Character* CharacterCreator::getCharacter() { return character; }

std::string CharacterCreator::getTitle() const { 
    return "Create your character\nPoints left: " + std::to_string(skillPoints) + 
            (character != NULL ? "\n" + character->shortDiscription() : ""); 
}
std::string CharacterCreator::getDescription() const { return phaseText.at(phase); }
std::list<std::string> CharacterCreator::getOptions() const { return phaseActions.at(phase)(); }
