#ifndef skill_h
#define skill_h

#include <string>
#include <list>
#include <map>

#include "data.h"
#include "identity.h"
#include "serializable.h"

/**
 * Class Skill implements Identity, Data, Serializable.
 * Represents a skill.
 */
class Skill : public Identity, public Data, public Serializable {
private:
    static std::map<std::string, Skill*> skills;
    
    std::string title;
    std::string description;

public:
    static const std::string hp;
    static void clear();
    static bool fromFile(std::ifstream& file);

    //throws std::out_of_bound
    static Skill* getSkill(const std::string& id);
    
    static std::list<std::string> getSkills();

    //throws Identity::UniqueArgumentException
    Skill(const std::string& title, const std::string& description);
    Skill(const std::string& id, const std::string& title, const std::string& description);

    bool toFile(std::ofstream& file) const;

    virtual std::string getTitle() const override;
    virtual std::string getDescription() const override;
    virtual std::list<std::string> getOptions() const;
};

/**
 * Class CharacterSkill implements Data.
 * Represents a skill of a character.
 */
class CharacterSkill : public Data {
private:
    /** levelMultiplier which sets points needed to achive new level */
    static const int lvlMultiplier;

    std::string skill;
    int maxLevel;
    int currentLevel;
    int regenerationPerTurn;
    int levelPoints;
    int levelPointsNeeded;

public:
    /**
     * Class LevelUpNotification implements Data.
     * Used as an exception, which notifies about a level up of a character.
     */
    class LevelUpNotification : public Data {
    private:
        std::string title;

    public:
        LevelUpNotification();
        LevelUpNotification(const std::string& title);

        friend LevelUpNotification* operator+ (const std::string& str, LevelUpNotification* lvlUp) { return new LevelUpNotification(str + " " + lvlUp->title); }

        virtual std::string getTitle() const;
        virtual std::string getDescription() const;
        virtual std::list<std::string> getOptions() const;
    };

    //throws std::out_of_bound
    CharacterSkill(const std::string& id, int maxLevel, int currLevel, int regenerationPerTurn = 10, int levelPoints = 0);
    CharacterSkill(const std::string& id, int maxLevel, int regenerationPerTurn = 10, int levelPoints = 0);

    void changeMaxBy(int points);
    void changeCurrBy(int points);
    void changeRegenerationBy(int points);
    int getCurrentLevel() const;
    int getNeededPoints() const;
    int getPoints() const;
    int getRegeneration() const;
    int getMaxLevel() const;
    void update();

    void checkLevelUp();

    virtual std::string getTitle() const override;
    virtual std::string getDescription() const override;
    virtual std::list<std::string> getOptions() const override;
};

#endif