#include <ctime> 

#include "army.h" 

Army::Army(Character* fighter, bool isControlled) 
: fighter(fighter), isControlledByPlayer(isControlled) {}

Character* Army::getCharacter() const { 
    return fighter; 
}

bool Army::isControlled() const { 
    return isControlledByPlayer; 
}

std::string Army::getTitle() const { 
    return getCharacter()->getTitle(); 
}

std::string Army::getDescription() const { 
    return fighter->shortDiscription(); 
}

std::list<std::string> Army::getOptions() const { 
    return {}; 
}