#ifndef fight_h
#define fight_h

#include <string>
#include <list>

#include "data.h"
#include "army.h"
#include "location.h"
#include "effect.h"

class Army;

/**
 * Class Fight implements Data
 * Contains an information about a fight of two Armies.
 */
class Fight : public Data {
private:
    enum Phase { FIRST_PLAYER = 0, SECOND_PLAYER = 1 };

    Army* first;
    Army* second;

    /** Abilities, which can be used by current player */
    std::list<Effect*> abilities;

    int phase;

public:
    Fight(Army* first, Army* second);

    void nextPhase();
    void updateAbilities();

    /**
     * Method getWinner return a pointer to the winning character, NULL if no one has won yet.
     * 
     * @return Pointer to the winning character.
     */
    Character* getWinner();

    void useEffectAt(int number);

    /**
     * Method makeMove makes a move in case a character is not controlled by a player.
     */
    void makeMove();

    Army* getTarget() const;
    Army* getAttacker() const;

    virtual std::string getTitle() const override;
    virtual std::string getDescription() const override;
    virtual std::list<std::string> getOptions() const override;
};

#endif