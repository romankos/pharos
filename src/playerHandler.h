#ifndef playerHandler_h
#define playerHandler_h

#include <string>

#include "inputHandler.h"
#include "characterCreator.h"

/**
 * Class PlayerHandler implements InputHandler.
 * Handles input in the context of Create Character Menu.
 */
class PlayerHandler : public InputHandler {
private:
    CharacterCreator* character;

public:
    
    PlayerHandler(CharacterCreator* character);

    virtual void handle(const std::string& data) const override;
};

#endif