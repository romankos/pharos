#ifndef genericData_h
#define genericData_h

#include <list>
#include "data.h"


/**
 * Class GenericData implements Data
 * Contains generic information needed for a game state.
 */
class GenericData : public Data {
private:
    std::string title;
    std::string description;
    std::list<std::string> options;
    
public:
    GenericData(const std::string& title, const std::string& description, const std::list<std::string>& options);
    virtual ~GenericData();
    virtual std::string getTitle() const override;
    virtual std::string getDescription() const override;
    virtual std::list<std::string> getOptions() const override;
};

#endif