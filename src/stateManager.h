#ifndef stateManager_h
#define stateManager_h 

#include <string>
#include <stack>
#include <queue>

#include "gameState.h"
#include "inputProvider.h"
#include "renderer.h"

class GameState;

/**
 * Class StateManager manages the stack of game states.
 */
class StateManager {
private:
    /** Manager's input provider */
    const InputProvider* inputProvider;
    /** Manager's common renderer provider */
    Renderer* currRenderer;
    
    /** Manager's game states stack */
    std::stack<GameState*> stateStack;
    /** Manager's queue of states to insert in a stack */
    std::queue<GameState*> statesToInsert;
    /** Manager's queue of states to delete*/
    std::queue<GameState*> statesToDelete;

public:
    StateManager(const InputProvider* inputProvider, Renderer* renderer);
    ~StateManager();
    
    /**
     * Method showInfo constructs a new state from data, pushes it on a stack, renders it and pops it right away.
     * 
	 * @param[in] data Data to render. 
     */
    void showInfo(Data* data);

     /**
     * Method showInfo constructs a new state from data and pushes it on a stack in NOTIFICATION MODE.
     * 
	 * @param[in] data Data to push. 
     */
    void showNotification(Data* data);

    /**
     * Method showError constructs a new state from data, pushes it on a stack in  ERROR MODE and pops it right away.
     * 
	 * @param[in] data Data to push. 
     */
    void showError(Data* data);

    void update();
    void render() const;

    /**
     * Method push constructs a new state from data and input handler and pushes it on a stack.
     * 
	 * @param[in] data Data to push. 
	 * @param[in] inputHandler InputHandler to handle data. 
     */
    void push(Data* data, InputHandler* inputHandler = NULL);

    /**
     * Method push pushes a state on a stack.
     * 
	 * @param[in] state Game State to push. 
     */
    void push(GameState* state);

    /**
     * Method push constructs a new state from data, renderer and input handler and pushes it on a stack.
     * 
	 * @param[in] data Data to push. 
	 * @param[in] renderer Renderer to render data. 
	 * @param[in] inputHandler InputHandler to handle data. 
     */
    void push(Data* data, Renderer* renderer,  InputHandler* inputHandler = NULL);

    /**
     * Method pop pops a state from stack.
     * 
     */
    void pop();

    /**
     * Method isEmpty checks if stack is empty.
     * Used to exut game loop when last stack has been poped.
     * 
	 * @return Flag indicating whether the stack is empty. 
     */
    bool isEmpty() const;

    /**
     * Method clear clears state stack.
     * 
     */
    void clear();
};


#endif