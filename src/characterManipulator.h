#ifndef characterManipulator_h
#define characterManipulator_h

#include <string>
#include <map>
#include <set>

#include "data.h"
#include "attitude.h"
#include "location.h"
#include "character.h"
#include "game.h"
#include "serializable.h"

class Location;
class Character;

/**
 * Class CharacterManipulator performs various manipulations over a Character object.
 * Can also act as a condition of effect.
 */
class CharacterManipulator : public Data, public Serializable {
public:
    enum Option { LESS = 0, EQUAL = 1, NONEQUAL = 2, MORE = 3 };

private:
    static std::map<int, Option> optionsMap;
    std::map<std::string, int> skills;
    std::map<std::string, bool> items;
    std::map<Attitude, Option> attitudes;
    Location* location;
    int chance;

    Option optionFromInt(int number);

public:
    bool fromFile(std::ifstream& file);

    /**
     * Constructs an object of CharacterManipulator from following arguments.
     * 
     * param[in] skills Skill and a value, by which it should be modified.
     * param[in] items Item and a flag whether it should be added or taken from.
     * param[in] attitudes Attitude and an Option, which a Character should fit.
     * param[in] location Needed location.
     * param[in] chance A probability of succussful usage of manipulation.
     */
    CharacterManipulator(
        const std::map<std::string, int>& skills = {},
        const std::map<std::string, bool>& items = {},
        const std::map<Attitude, Option>& attitudes = {},
        Location* location = NULL,
        int chance = 100);
    
    bool canBeUsed(Character* user, Character* target);

    bool use(Character* user, Character* target);

    bool toFile(std::ofstream& file) const override;

    std::string toLine() const;

    virtual std::string getTitle() const override;
    virtual std::string getDescription() const override;
    virtual std::list<std::string> getOptions() const override;

};

#endif