#include <sstream>
#include "characterManipulator.h"


std::map<int, CharacterManipulator::Option> CharacterManipulator::optionsMap = {{0, CharacterManipulator::LESS},
                                                                                {1, CharacterManipulator::EQUAL},
                                                                                {2, CharacterManipulator::NONEQUAL},
                                                                                {3, CharacterManipulator::MORE}};
CharacterManipulator::Option CharacterManipulator::optionFromInt(int number) {
    try { return optionsMap.at(number); } catch (const std::out_of_range& e) { return EQUAL; }
}

bool CharacterManipulator::fromFile(std::ifstream& file) {
    std::string tmp;
    if(!file.good()) { return false; }
    while(tmp != "Skills:" && !file.eof()) { std::getline(file, tmp); }
    std::getline(file, tmp);
    while (tmp != "Items:" && !file.eof()) {
        if (tmp != "") {
            std::istringstream iss(tmp);
            std::string skillName; iss >> skillName;
            int points; iss >> points;
            skills.emplace(skillName, points);
        }
        std::getline(file, tmp);
    }
    std::getline(file, tmp);
        while (tmp != "Attitudes:" && !file.eof()) {
        if (tmp != "") {
            std::istringstream iss(tmp);
            std::string item; iss >> item;
            bool flag; iss >> flag;
            items.emplace(item, flag);
        }
        std::getline(file, tmp);
    }
    std::getline(file, tmp);
        while (tmp != "Chance:" && !file.eof()) {
        if (tmp != "") {
            std::istringstream iss(tmp);
            std::string attitude; iss >> attitude;
            int option; iss >> option;
            attitudes.emplace(attitudeFromString(attitude), optionFromInt(option));
        }
        std::getline(file, tmp);
    }
    std::getline(file, tmp);
    chance = std::stoi(tmp);
    if (!file.good()) { return false; }
    return file.good();
}

CharacterManipulator::CharacterManipulator(
    const std::map<std::string, int>& skills,
    const std::map<std::string, bool>& items,
    const std::map<Attitude, Option>& attitudes,
    Location* location,
    int chance) : skills(skills), items(items), attitudes(attitudes), location(location), chance(chance) {
    if (chance < 0) { this->chance = 0; }
    else if (chance > 100) { this->chance = 100; }
}

bool CharacterManipulator::canBeUsed(Character* user, Character* target) {
    try {
        if(user == NULL || target == NULL) { return false; }
        for (auto e : skills) { if(user->getSkill(e.first)->getCurrentLevel() < e.second) { return false; } }
        for (auto e : items) { if(!user->hasItem(e.first)) { return false; } }
        for (auto e : attitudes) {
            if(e.second == MORE     && e.first >  user->getAttitude(target)) { return false; }
            if(e.second == LESS     && e.first <  user->getAttitude(target)) { return false; }
            if(e.second == EQUAL    && e.first != user->getAttitude(target)) { return false; }
            if(e.second == NONEQUAL && e.first == user->getAttitude(target)) { return false; }
        }
        if(location != NULL && location->getTitle() != Location::getLocationOf(user)->getTitle()) { return false; }
    } catch (const std::out_of_range& e) { return false; }
    return true;
}

bool CharacterManipulator::use(Character* user, Character* target) {
    srand(time(0));
    int number = rand() % 101;
    if (number <= chance) {
        for (auto e : skills) {
            try { target->getSkill(e.first)->changeCurrBy(e.second); } 
            catch (const std::out_of_range& e) {}
            catch (CharacterSkill::LevelUpNotification* e) { Game::getCurrentGame()->getManager()->showNotification(target->getTitle() + e); }
        }
        for (auto e : items) {
            if (e.second) { target->addItem(e.first); } else { target->deleteItem(e.first); }
        }
        for (auto e : attitudes) { target->setAttitude(user->getId(), e.first); }
        if (location != NULL) { Location::removeCharacterFromAll(target); location->addCharacter(target->getId()); }
        return true;
    } else { return false; }
}

bool CharacterManipulator::toFile(std::ofstream& file) const {
    file << "Skills:\n";
    for (auto e : skills) { file << e.first << " " << e.second << "\n"; }
    file << "Items:\n";
    for (auto e : items) { file << e.first << " " << e.second << "\n"; }
    file << "Attitudes:\n";
    for (auto e : attitudes) { file << attitudeToString(e.first) << " " << e.second << "\n"; }
    file << "Chance:\n" << chance << "\n";
    return file.good();
}

std::string CharacterManipulator::getTitle() const {return "Character Manipulator"; }

std::string CharacterManipulator::getDescription() const {
    std::string result = skills.size() > 0 ? "\tSkills: " : "";
    for (auto e : skills) { result += e.first + " " + std::to_string(e.second) + "; "; }
    result += skills.size() > 0 ? "\n" : "";
    result += items.size() > 0 ? "\tItems: " : "";
    for (auto e : items) { result += e.first + (e.second ? "+ " : "- "); }
    result += items.size() > 0 ? "\n" : "";
    result += location != NULL ? "\tLocation: " + location->getTitle() + "\n": "";
    result += attitudes.size() > 0 ? "\tAttitude: " : "";
    for (auto e : attitudes) { result += attitudeToString(e.first) + (e.second == LESS ? "- " : (e.second == MORE ? "+ " : " ")); }
    result += attitudes.size() > 0 ? "\n" : "";
    result += chance != 100 ? "\tChance: " + std::to_string(chance) + '%' : "";  
    return result;
}

std::string CharacterManipulator::toLine() const {
    std::string result = skills.size() > 0 ? "Skills: " : "";
    for (auto e : skills) { result += e.first + " " + std::to_string(e.second) + "; "; }
    result += items.size() > 0 ? "Items: " : "";
    for (auto e : items) { result += Item::getItem(e.first)->getTitle() + (e.second ? "+ " : "- "); }
    result += location != NULL ? " Location: " + location->getTitle() + " ": "";
    result += attitudes.size() > 0 ? "Attitude: " : "";
    for (auto e : attitudes) { result += attitudeToString(e.first) + (e.second == LESS ? "- " : (e.second == MORE ? "+ " : " ")); }
    result += chance != 100 ? "Chance: " + std::to_string(chance) + '%' : "";  
    return result;
}

std::list<std::string> CharacterManipulator::getOptions() const { return {}; }
