#include "dialogHandler.h"
#include "effect.h"


DialogHandler::DialogHandler(Character* talking, Character* talkedTo, const std::list<std::string>& options) 
: talking(talking), talkedTo(talkedTo) {
    int i = 1;
    for (auto option : options) {
        this->options[i++] = option;
    }
}

void DialogHandler::handle(const std::string& data) const {

    int number = std::stoi(data);
    if (number == 0) { Game::getCurrentGame()->getManager()->pop(); return; }
    if(options.at(number) == "Tell me about yourself") {
        Game::getCurrentGame()->getManager()->showInfo(talkedTo);
        return;
    } else if(options.at(number) == "Exit") {
        Game::getCurrentGame()->getManager()->pop();
        return;
    } else if (Effect::getEffect(options.at(number)) != NULL) {
        Effect::getEffect(options.at(number))->use(talking, talkedTo);
        Game::getCurrentGame()->getManager()->pop();
        return;
    }

    throw std::out_of_range("");
}
