#include <cstring>
#include <sys/stat.h>
#include <dirent.h>

#include "fileManager.h"
#include "genericData.h"
#include "item.h"
#include "effect.h"
#include "skill.h"
#include "character.h"
#include "location.h"
#include "game.h"


const std::string FileManager::src = "srcSave";
const std::string FileManager::path = "examples/saves/";


bool FileManager::loadGame(const std::string& save) {

    Clearable::clear();

    try {

    Item::clear();
    std::ifstream fileInItem(path + save + "/items.src", std::fstream::in);
    if(!Item::fromFile(fileInItem)) {
        Game::getCurrentGame()->getManager()->showError(new GenericData ("Error reading items!", "", {}));
        return false;
    }
    fileInItem.close();

    Skill::clear();
    std::ifstream fileInSkill(path + save + "/skills.src", std::fstream::in);
    if(!Skill::fromFile(fileInSkill)) {
        Game::getCurrentGame()->getManager()->showError(new GenericData ("Error reading skills!", "", {}));
        return false;
    }
    fileInSkill.close();

    Character::clear();
    std::ifstream fileInChar(path + save + "/characters.src", std::fstream::in);
    if(!Character::fromFile(fileInChar)) {
        Game::getCurrentGame()->getManager()->showError(new GenericData ("Error reading characters!", "", {}));
        return false;
    }
    fileInChar.close();

    Location::clear();
    std::ifstream fileInLoc(path + save + "/locations.src", std::fstream::in);
    if(!Location::fromFile(fileInLoc)) {
        Game::getCurrentGame()->getManager()->showError(new GenericData ("Error reading locations!", "", {}));
        return false;
    }
    fileInLoc.close();

    Effect::clear();
    std::ifstream fileInEff(path + save + "/effects.src", std::fstream::in);
    if(!Effect::fromFile(fileInEff)) {
        Game::getCurrentGame()->getManager()->showError(new GenericData ("Error reading effects!", "", {}));
        return false;
    }
    fileInEff.close();

    std::ifstream fileInGame(path + save + "/game.src", std::fstream::in);
    if(!Game::getCurrentGame()->fromFile(fileInGame)) {
        Game::getCurrentGame()->getManager()->showError(new GenericData ("Error reading game!", "", {}));
        return false;
    }
    fileInGame.close();

    } catch (Data* e) { Game::getCurrentGame()->getManager()->showError(e); return false; }

    return true;
}


bool FileManager::saveGame(const std::string& save) {
    
    bool isNew = true;
    for (auto e : getSaves()) { 
        if (save == e) { 
            Game::getCurrentGame()->getManager()->showInfo(new GenericData ("Rewriting your existing save!", "", {}));
            isNew = false;
            break;
        }
    }    

    if (isNew) {
        Game::getCurrentGame()->getManager()->showInfo(new GenericData ("Creating new save!", "", {}));
        mkdir((path + save).c_str(), S_IRWXU | S_IRWXG | S_IROTH );
    }

    std::ofstream fileOutGame(path + save + "/game.src", std::fstream::out);
    if (!Game::getCurrentGame()->toFile(fileOutGame)) {
            Game::getCurrentGame()->getManager()->showError(new GenericData ("Error writing game!", "", {}));
            fileOutGame.close();                    
            return false;
        }
    fileOutGame.close();
        
    std::ofstream fileOutItem(path + save + "/items.src", std::fstream::out);
    for(auto e : Item::getItems()) { 
        if (!Item::getItem(e)->toFile(fileOutItem)) {
            Game::getCurrentGame()->getManager()->showError(new GenericData ("Error writing items!", "", {}));
            fileOutItem.close();                    
            return false;
        }
    }
    fileOutItem.close();

    std::ofstream fileOutSkill(path + save + "/skills.src", std::fstream::out);
    for(auto e : Skill::getSkills()) { 
        if(!Skill::getSkill(e)->toFile(fileOutSkill)) {
            Game::getCurrentGame()->getManager()->showError(new GenericData ("Error writing skills!", "", {}));
            fileOutSkill.close();
            return false;
        }
    }
    fileOutSkill.close();

    std::ofstream fileOutChar(path + save + "/characters.src", std::fstream::out);
    for(auto e : Character::getCharacters()) { 
        if(!Character::getCharacter(e)->toFile(fileOutChar)) {
            Game::getCurrentGame()->getManager()->showError(new GenericData ("Error writing characters!", "", {}));
            fileOutChar.close();
            return false;
        }
    }
    fileOutChar.close();

    std::ofstream fileOutLoc(path + save + "/locations.src", std::fstream::out);
    for(auto e : Location::getLocations()) { 
        if(!Location::getLocation(e)->toFile(fileOutLoc)) {
            Game::getCurrentGame()->getManager()->showError(new GenericData ("Error writing locations!", "", {}));
            fileOutLoc.close();
            return false;
        }
    }
    fileOutLoc.close();

    std::ofstream fileOutEffects(path + save + "/effects.src", std::fstream::out);
    for(auto e : Effect::getEffects()) {
        if(!Effect::getEffect(e)->toFile(fileOutEffects)) {
            Game::getCurrentGame()->getManager()->showError(new GenericData ("Error writing effects!", "", {}));
            fileOutEffects.close();
            return false;
        }
    }
    fileOutEffects.close();
    return true;
}


std::list<std::string>& FileManager::getSaves() {
    DIR* dirp = opendir(FileManager::path.c_str());
    dirent* dp;
    std::set<std::string> result;
    char excludions[3][8] = {".", "..", "srcSave"};

    while ((dp = readdir(dirp)) != NULL) {
        size_t len = std::strlen(dp->d_name);
        bool flag = false;
        for (auto e : excludions) { 
            if (len == std::strlen(e) && !strcmp(dp->d_name, e)) {
                flag = true;
            }
        }
        if (!flag) result.emplace(dp->d_name);
    }

    closedir(dirp);

    saves = std::list<std::string>(result.begin(), result.end());
    saves.emplace_back("Exit");

    // delete dirp;
    delete dp;

    return saves;
}