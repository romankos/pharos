#ifndef data_h
#define data_h

#include <string>
#include <list>

#include "clearable.h"

/**
 * Class Data provides an interfase for data to be rendered by the Renderer.
 * Class Data extends Clearable.
 */
class Data : public Clearable {
public:
    virtual ~Data() {}

    /**
     * Method update updates the state if Data if nessecary.
     */
    virtual void update() {};

    /**
     * Method getTitle return the title of the Data as a string
     * 
	 * @return title as a string
     */
    virtual std::string getTitle() const = 0;

    /**
     * Method getDescription return the description of the Data as a string
     * 
	 * @return description as a string
     */
    virtual std::string getDescription() const = 0;

    /**
     * Method getOptions return the options data provides
     * 
	 * @return options as a list of strings
     */
    virtual std::list<std::string> getOptions() const = 0;
};

#endif