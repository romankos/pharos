#include "genericData.h" 

GenericData::GenericData(const std::string& title, const std::string& description, const std::list<std::string>& options) : title(title), description(description), options(options) {}
GenericData::~GenericData() {}
std::string GenericData::getTitle() const { 
    return title; 
}
std::string GenericData::getDescription() const { 
    return description; 
}
std::list<std::string> GenericData::getOptions() const { 
    return options; 
}