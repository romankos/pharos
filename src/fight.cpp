#include <string>
#include <list>

#include "fight.h"

Fight::Fight(Army* first, Army* second) 
    : first(first), second(second) { 
    first->getCharacter()->setAttitude(second->getCharacter()->getId(), ENEMY);
    second->getCharacter()->setAttitude(first->getCharacter()->getId(), ENEMY);
    phase = -1; nextPhase();
    while(!getAttacker()->isControlled()) { nextPhase(); }
}


void Fight::nextPhase() { 
    phase++; 
    updateAbilities(); 
}

void Fight::updateAbilities() { 
    abilities = Effect::getEffects(getAttacker()->getCharacter(), getTarget()->getCharacter());
    auto additions = Effect::getEffects(getAttacker()->getCharacter());
    std::set<Effect*> setAbilities(abilities.begin(), abilities.end());
    setAbilities.insert(additions.begin(), additions.end());
    abilities = std::list<Effect*>(setAbilities.begin(), setAbilities.end());
}

Character* Fight::getWinner() {
    if(getTarget()->getCharacter()->getSkill(Skill::hp)->getCurrentLevel() <= 0) { 
        return getAttacker()->getCharacter(); 
    }
    return NULL;
}

void Fight::useEffectAt(int number) {
    if(number > (int)abilities.size()) { throw std::out_of_range(""); }
    (*std::next(abilities.begin(), number - 1))->use(getAttacker()->getCharacter(), getTarget()->getCharacter());
}

void Fight::makeMove() {
    srand(time(0));
    int number = rand() % (abilities.size()) + 1;
    
    useEffectAt(number);
}


Army* Fight::getTarget() const { return phase % 2 ? first : second; }
Army* Fight::getAttacker() const { return phase % 2 ? second : first; }

std::string Fight::getTitle() const { 
    return first->getTitle() + " vs " + second->getTitle() + "; Location: " + Location::getLocationOf(getAttacker()->getCharacter())->getTitle(); 
}

std::string Fight::getDescription() const { 
    return getAttacker()->getDescription() + "\n" + getTarget()->getDescription(); 
}

std::list<std::string> Fight::getOptions() const { 
    if (getAttacker()->isControlled()) {
        std::list<std::string> options;
        for (auto e : abilities) { options.emplace_back(e->getTitle() + "\n" + e->getDescription()); }
        return options;
    }
    return {};
}


