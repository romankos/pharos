#ifndef attitude_h
#define attitude_h

#include <string>

/**
 * Enum Attitude provides possible attitudes of one Character to another.
 */
enum Attitude { 
    YOURSELF = -5, DEAD = -4, ENEMY = -3, AFRAID = -2, UNFRIENDLY = -1, NEUTRAL = 0, FRIENDLY = 1, ALLY = 2
};

/**
 * Method attitudeToString converts Attitude to string.
 * 
 * @return Attitude as a string.
 */
std::string attitudeToString(Attitude a);

/**
 * Method attitudeFromString readsAttitudeFromString.
 * 
 * @return Attitude read from string.
 */
Attitude attitudeFromString(std::string src);

#endif