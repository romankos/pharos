#include "renderer.h"

Renderer::~Renderer() {}

Renderer::MODE Renderer::getMode() const { return mode; }

Renderer* Renderer::setMode(Renderer::MODE mode) {
    this->mode = mode;
    return this;
}
