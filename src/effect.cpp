#include <map>

#include "effect.h"
#include "game.h"
#include "genericData.h"


std::map<std::string, Effect*> Effect::effects = {};

void Effect::clear() { effects.clear(); }

bool Effect::fromFile(std::ifstream& file) {
    std::string id;
    std::string title;
    std::string tmp;
    CharacterManipulator* condition;
    CharacterManipulator* effectOnTarget;
    CharacterManipulator* effectOnUser;

    if(!file.good()) { return false; }
    while(tmp != "ID:" && !file.eof()) { std::getline(file, tmp); }
    while (!file.eof()) {
        id = title = tmp = "";
        condition = new CharacterManipulator();
        effectOnTarget = new CharacterManipulator();
        effectOnUser = new CharacterManipulator();
        std::getline(file, tmp);
        while (tmp != "Title:" && !file.eof()) {
            if (tmp != "") id += tmp + "\n";
            std::getline(file, tmp);
        }
        std::getline(file, tmp);
            while (tmp != "Condition:" && !file.eof()) {
            if (tmp != "") title += tmp + "\n";
            std::getline(file, tmp);
        }
        if (id != "") id.pop_back();
        if (title != "") title.pop_back();
        if (!condition->fromFile(file)) return false;
        std::getline(file, tmp);
        if (tmp != "Target:") return false;
        if (!effectOnTarget->fromFile(file)) return false;
        std::getline(file, tmp);
        if (tmp != "User:") return false;
        if (!effectOnUser->fromFile(file)) return false;
        while(tmp != "ID:" && !file.eof()) { std::getline(file, tmp); }
        new Effect(id, title, condition, effectOnTarget, effectOnUser);
        if (!file.good() && !file.eof()) { return false; }
    }
    return file.good() || file.eof();
}

std::list<std::string> Effect::getEffects() {
    std::list<std::string> result;
    for (auto e : effects) { result.emplace_back(e.first); }
    return result;
}

std::list<Effect*> Effect::getEffects(Character* user) {
    return getEffects(user, user);
}

std::list<Effect*> Effect::getEffects(Character* user, Character* target) {
    std::list<Effect*> result;
    for (auto e : effects) { if(e.second->canBeUsed(user, target)) { result.emplace_back(e.second); }}
    return result;
}

Effect* Effect::getEffect(const std::string& id) { return effects.at(id); }


Effect::Effect (const std::string& title, CharacterManipulator* condition, CharacterManipulator* effectOnTarget, CharacterManipulator* effectOnUser) :
Effect(title, title, condition, effectOnTarget, effectOnUser) {}

Effect::Effect (const std::string& id, const std::string& title, CharacterManipulator* condition, CharacterManipulator* effectOnTarget, CharacterManipulator* effectOnUser) :
Identity(id), title(title), condition(condition), effectOnTarget(effectOnTarget), effectOnUser(effectOnUser) {
    if(!effects.emplace(id, this).second) { throw Identity::UniqueArgumentException("Effect with an id: " + id + " already exists"); }
}

bool Effect::canBeUsed(Character* user, Character* target) {
    return condition->canBeUsed(user, target);
}

void Effect::use(Character* user, Character* target) {
    if(effectOnTarget->use(user, target)) {
        Game::getCurrentGame()->getManager()->showInfo(new GenericData(user->getTitle() + " used " + title + " on " + target->getTitle() + "!", "", {}));
    } else {
        Game::getCurrentGame()->getManager()->showInfo(new GenericData(user->getTitle() + " failed to use " + title + " on " + target->getTitle() + "!", "", {}));
    }
    if(!effectOnUser->use(user, user)) {
        Game::getCurrentGame()->getManager()->showInfo(new GenericData(user->getTitle() + " failed to use " + title + " on " + user->getTitle() + "!", "", {}));
    }
}

bool Effect::toFile(std::ofstream& file) const {
    file << "ID:\n" << getId() << "\n";
    file << "Title:\n" << getTitle() << "\n";
    file << "Condition:\n";
    condition->toFile(file);
    file << "Target:\n";
    effectOnTarget->toFile(file);
    file << "User:\n";
    effectOnUser->toFile(file);
    return file.good();
}

std::string Effect::getTitle() const { return title; }

 std::string Effect::getDescription() const {
    std::string result = effectOnTarget->toLine();
    if(result != "") { result = "    To target - " + result; }
    std::string tmp1 = effectOnUser->toLine();
    if (tmp1 != "") { if (result != "") { result += "\n"; } result += "    To user   - " + tmp1; }
    std::string tmp2 = condition->toLine();
    if (tmp2 != "") { if (result != "") { result += "\n"; } result += "    Requires  - " + tmp2; }
    return result;
}

std::list<std::string> Effect::getOptions() const { return {}; }

