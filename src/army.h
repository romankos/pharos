#ifndef army_h
#define army_h

#include <string>
#include <list>

#include "data.h"
#include "character.h"


/**
 * Class Army implements Data
 * Acst as a wrapper over Character during a Fight.
 */
class Army : public Data {
private:
    Character* fighter;
    bool isControlledByPlayer;

public:
    /**
     * Constructs an object of Army.
     * 
	 * @param[in] fighter Character in a fight
	 * @param[in] isControlled Flag showing whether a fighter is controlled by player or computer.
     */
    Army(Character* fighter, bool isControlled = false);
    
    Character* getCharacter() const;
    bool isControlled() const;

    std::string getTitle() const override;
    std::string getDescription() const override;
    std::list<std::string> getOptions() const override;
};

#endif