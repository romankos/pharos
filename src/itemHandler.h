#ifndef itemHandler_h
#define itemHandler_h

#include <map>
#include <list>

#include "inputHandler.h"
#include "character.h"
#include "game.h"

class Game;

/**
 * Class ItemHandler implements InputHandler.
 * Handles input in the context of Item Picker Menu.
 */
class ItemHandler : public InputHandler {
private:
    Character* character;
    std::map<int, std::string> items;

public:

    /**
     * Constructs an object of ItemHandler from character to pick an item and list of items.
     * 
	 * @param[in] character Character to pick an item. 
	 * @param[in] items List of items to pick up. 
     */
    ItemHandler(Character* character, const std::list<std::string>& items);
    virtual void handle(const std::string& data) const override;
};

#endif