#ifndef characerCreator_h
#define characerCreator_h

#include <string>
#include <map>
#include <list>

#include "data.h"
#include "character.h"

/**
 * Class CharacterCreator implements Data.
 * A Helper class responsible for creating a new character.
 */
class CharacterCreator : public Data {
private:
    typedef std::list<std::string> (*function)(void);

    Character* character;
    std::map<int, std::string> phaseText;
    std::map<int, function> phaseActions;
    int phase;
    int finalPhase;
    int skillPoints;

public:
    CharacterCreator(const std::map<int, std::string>& phaseText, const std::map<int, function>& phaseActions);

    void create(const std::string& name);
    void update();
    int getPhase();
    int getSkillPoints();
    int deduceSkillPoints(int points);
    bool isFinished();
    Character* getCharacter();

    virtual std::string getTitle() const override;
    virtual std::string getDescription() const override;
    virtual std::list<std::string> getOptions() const override;

};

#endif