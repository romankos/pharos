#ifndef inputHandler_h
#define inputHandler_h

#include "data.h"

/**
 * Class InputHandler provides an interface for handling given input.
 */
class InputHandler {
public:
    virtual ~InputHandler() {}

    /**
	 * Method handle handles given input.
	 * 
	 * @param[in] data Given input as a string.
	 */
    virtual void handle(const std::string& data) const = 0;
};

#endif