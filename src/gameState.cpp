#include <iostream>

#include "gameState.h"
#include "genericData.h"
#include "game.h"

GameState::GameState(Data* data, Renderer* renderer, const InputHandler* input) 
: dataComponent(data), renderComponent(renderer), inputComponent(input) {}

GameState::~GameState() { delete inputComponent; delete renderComponent; }

void GameState::enter() const {}

void GameState::render() const { renderComponent->render(dataComponent); }

void GameState::update() { dataComponent->update(); }

void GameState::handleInput(std::string data) const { 
    try { inputComponent->handle(data); } 
    catch (const std::out_of_range& e) { error(new GenericData("Choose one of the presented options!", "", {})); }
    catch (const std::invalid_argument& e) { error(new GenericData("Input a number!", "", {})); }
    catch (Data* e) { Game::getCurrentGame()->getManager()->showError(e); }
}

void GameState::error(Data* data) const { 
    Renderer::MODE old = renderComponent->getMode();
    renderComponent->setMode(Renderer::ERROR); 
    renderComponent->render(data); 
    renderComponent->setMode(old);
}

void GameState::exit() const {}