#include <sstream>
#include <fstream>

#include "character.h"
#include "game.h"

const int Character::itemCount = 4;

std::map<std::string, Character*> Character::characters = {};

void Character::clear() { 
    characters.clear();
}

bool Character::fromFile(std::ifstream& file) {
    std::string id;
    std::string title;
    std::string greeting;
    std::string tmp;
    if(!file.good()) { return false; }
    while(tmp != "ID:" && !file.eof()) { std::getline(file, tmp); }
    while (!file.eof()) {
        id = title = greeting = tmp = "";
        std::getline(file, tmp);
        while (tmp != "Title:" && !file.eof()) {
            if (tmp != "") id += tmp + "\n";
            std::getline(file, tmp);
        }

        std::getline(file, tmp);
            while (tmp != "Description:" && !file.eof()) {
            if (tmp != "") title += tmp + "\n";
            std::getline(file, tmp);
        }

        std::getline(file, tmp);
            while (tmp != "Skills:" && !file.eof()) {
            if (tmp != "") greeting += tmp + "\n";
            std::getline(file, tmp);
        }
        if (id != "") id.pop_back();
        if (title != "") title.pop_back();
        if (greeting != "") greeting.pop_back();
        Character* newChar = new Character(id, title, greeting);
        std::getline(file, tmp);
        while (tmp != "Attitudes:" && !file.eof()) {
            if (tmp != "") {
                std::istringstream iss(tmp);
                std::string skillName; iss >> skillName;
                int maxLevel; iss >> maxLevel;
                int currLevel; iss >> currLevel;
                int regeneration; iss >> regeneration;
                int points; iss >> points;
                newChar->addSkill(new CharacterSkill(skillName, maxLevel, currLevel, regeneration, points));
            }
            std::getline(file, tmp);
        }

        std::getline(file, tmp);
        while (tmp != "Items:" && !file.eof()) {
            if (tmp != "") {
                std::istringstream iss(tmp);
                std::string character; iss >> character;
                std::string attitude; iss >> attitude;
                newChar->setAttitude(character, attitudeFromString(attitude));
            }
            std::getline(file, tmp);
        }

        std::getline(file, tmp);
        while (tmp != "ID:" && !file.eof()) {
            if (tmp != "") newChar->addItem(tmp);
            std::getline(file, tmp);
        }

        if (!file.good() && !file.eof()) { return false; }

    }
    return file.good() || file.eof();
}

bool Character::toFile(std::ofstream& file) const {
    file << "ID:\n" + getId() + "\nTitle:\n" + getTitle() + "\nDescription:\n" + getGreeting() + "\nSkills:\n";
    for (auto e : skills) { file << e.first << " " << e.second->getMaxLevel() << " " << e.second->getCurrentLevel() << " " << e.second->getRegeneration() << " " << e.second->getPoints() << "\n"; }
    file << "Attitudes:\n";
    for (auto e : attitudes) { file << e.first << " " << attitudeToString(e.second) << "\n"; }
    file << "Items:\n";
    for (auto e : items) { file << e << "\n"; }
    return file.good();
}


Character* Character::getCharacter(const std::string& id) { return characters.at(id); }

std::list<std::string> Character::getCharacters() {
    std::list<std::string> result;
    for(auto e : characters) { result.emplace_back(e.first); }
    return result;
}

Character::Character(const std::string& title, const std::string& greeting) 
: Character::Character(title, title, greeting) {}

Character::Character(const std::string& id, const std::string& title, const std::string& greeting) : Identity(id), title(title), greeting(greeting) { 
    if(!characters.emplace(id, this).second) { throw Identity::UniqueArgumentException("Character with an id" + getId() + " already exists!"); }
    this->setAttitude(id, YOURSELF);
}

bool Character::addSkill(CharacterSkill* skill) {
    return skills.emplace(skill->getTitle(), skill).second;
}

CharacterSkill* Character::getSkill(const std::string& id) const {
    return skills.at(id);
}

bool Character::hasItem(const std::string& id) const {
    return items.find(id) != items.end();
}

bool Character::addItem(const std::string& id) {
    if(items.size() < itemCount || hasItem(id)) {
        items.emplace(id);
        return true;
    } else { return false; }
}

void Character::deleteItem(const std::string& id) {
    items.erase(id);
}

std::list<std::string> Character::getItems() const {
    std::list<std::string> result;
    for (auto e : items) { result.emplace_back(e); }
    return result;
}

Attitude Character::getAttitude(Character* character) const {
    try { return attitudes.at(character->getId()); }
    catch (const std::out_of_range& e) { return NEUTRAL; }
}

void Character::setAttitude(const std::string& id, Attitude attitude) { attitudes[id] = attitude; }

void Character::update() {
    for(auto e : skills) { e.second->update(); }
}

std::string Character::getGreeting() const { return greeting; }

std::string Character::getTitle() const { return title; }

std::string Character::getDescription() const {
    std::string result = greeting;
    result += result != "" ? "\n" : "";
    result += "Attitude towards you: " + attitudeToString(getAttitude(Game::getCurrentGame()->getCharacter())); 
    for (auto e : skills) { 
        result += "\nSkill: " + e.second->getTitle() + " - " + std::to_string(e.second->getCurrentLevel()) + "; ";
        std::stringstream ss(e.second->getDescription());
        std::string to;
        std::getline(ss, to, '\n');
        while(std::getline(ss, to, '\n')) {
            result += "\n       " + to;
        }
    }
    result += "\nItems: ";
    for (auto e : items) { result += Item::getItem(e)->getTitle() + ":\n       " + Item::getItem(e)->getDescription() + "\n       "; }
    for (int i = 0; i < 8; i++) { result.pop_back(); }
    return result;
}

std::string Character::shortDiscription() const {
    std::string result = getTitle();
    for (auto e : skills) { result += " " + e.second->getTitle() + ": " + std::to_string(e.second->getCurrentLevel()) + ";";}
    return result;
}

std::list<std::string> Character::getOptions() const { return {}; }
