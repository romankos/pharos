#include "clearable.h"

std::unordered_set<Clearable*> Clearable::data = {};
void Clearable::clear() { 
    for(Clearable* el : data) { 
        if (el != NULL ) { delete el; } 
    }  
    data.clear(); 
}

Clearable::Clearable() { data.emplace(this); }