#include <csignal>
#include <string>

#include "consoleProvider.h"
#include "consoleRenderer.h"
#include "stateManager.h"
#include "gameState.h"
#include "location.h"
#include "locationHandler.h"
#include "skill.h"
#include "clearable.h"
#include "fight.h"
#include "fightHandler.h"
#include "effect.h"
#include "army.h"
#include "item.h"
#include "characterCreator.h"
#include "playerHandler.h"
#include "characterManipulator.h"
#include "genericInputHandler.h"
#include "fileManager.h"

void clear() {
    delete Game::getCurrentGame();
    Clearable::clear();
}

void signalHandler ( int ) {
    clear();
    exit( 0 );
}

int main ( void ) {

    // InputProvider* provider = new ConsoleProvider();
    // Renderer* renderer = new ConsoleRenderer();

    // std::ifstream fileInItem("saves/srcSave/items.src");
    // Item::fromFile(fileInItem);
    // fileInItem.close();

    // std::cout << "itemsRead" << std::endl;

    // std::ifstream fileInSkill("saves/srcSave/skills.src");
    // Skill::fromFile(fileInSkill);
    // fileInSkill.close();

    // std::cout << "skillsRead" << std::endl;

    // std::ifstream fileInChar("saves/srcSave/characters.src");
    // Character::fromFile(fileInChar);
    // fileInChar.close();

    // std::cout << "charactersRead" << std::endl;

    // std::ifstream fileInLoc("saves/srcSave/locations.src");
    // Location::fromFile(fileInLoc);
    // fileInLoc.close();

    // std::cout << "locationssRead" << std::endl;

    // std::ifstream fileInEff("saves/srcSave/effects.src");
    // Effect::fromFile(fileInEff);
    // fileInEff.close();

    // std::cout << "effectsRead" << std::endl;


    // Location* desert = new Location("Desert", "There is nothing here... Not a soul...");
    // Location* river = new Location("Desert_River", "Desert River", "River in the desert, but no living thing seems to be attracted to it...");
    // Location* walls = new Location("Pharos_Walls", "Pharos Walls", "The Walls are protecting the city from threats yet unknown to you.");
    // Location* city = new Location("Pharos", "The magnificent city in the middle of desert built by misterious shadow-like people");
    // Location* start = new Location("Start", "Welcome!", "You may now proceed further...");

    // Character* shadow = new Character("Shadow", "...");
    // Character* villager = new Character("Villager", "Hello, young fellow! Come later, I'm busy now.");
    // Character* monk = new Character("Monk", "Belive in God and He will be with you...");
    // Character* knight = new Character("Knight", "Nothing can save you from my sword!");

    // shadow->setAttitude(monk, ENEMY);
    // villager->setAttitude(monk, ENEMY);

    // new Skill("Energy", "Basically your health");
    // new Skill("Faith", "Your optimism level");
    // new Skill("Charisma", "How good you are at persuasion");
    
    // new Item("Magic_Wand", "Magic Wand", "Gives you additional magical powers"); 
    // new Item("Hammer", "Can be very useful, but requires a lot of strength"); 
    // new Item("Knife", "Fast melee weapon"); 
    // new Item("Stick", "Useless item"); 
    // new Item("Stone", "A stone to throw"); 
    // new Item("Potion", "Drink to restore energy"); 



    // new Effect("Punch", 
    //     new CharacterManipulator({{"Energy", 30}}, {}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator({{"Energy", -20}}, {}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator({{"Energy", -5}}));
    // new Effect("Holy_Blast", "Holy Blast", 
    //     new CharacterManipulator({{"Faith", 90}}, {}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator({{"Energy", -30}}, {}, {{ENEMY, CharacterManipulator::EQUAL}}, NULL, 60), 
    //     new CharacterManipulator({{"Energy", -5}, {"Faith", -10}}));
    // new Effect("Throw_sand", "Throw sand", 
    //     new CharacterManipulator({}, {}, {{ENEMY, CharacterManipulator::EQUAL}}, Location::getLocation("Desert")), 
    //     new CharacterManipulator({{"Energy", -10}}, {}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator());
    // new Effect("Nothing", 
    //     new CharacterManipulator(), 
    //     new CharacterManipulator(), 
    //     new CharacterManipulator());
    // new Effect("Enchantment",    
    //     new CharacterManipulator({}, {{"Magic_Wand", true}}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator({{"Energy", -20}}, {}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator({{"Faith", -10}}));
    // new Effect("Faith_Drain", "Faith Drain",    
    //     new CharacterManipulator({{"Faith", 150}}, {{"Magic_Wand", true}}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator({{"Faith", -200}}, {}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator({{"Faith", -30}}));
    // new Effect("Magical_Healing", "Magical Healing",    
    //     new CharacterManipulator({}, {{"Magic_Wand", true}}, {{YOURSELF, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator(), 
    //     new CharacterManipulator({{"Faith", -10}, {"Energy", 20}}));
    // new Effect("Potion_Healing", "Potion Healing",    
    //     new CharacterManipulator({}, {{"Potion", true}}, {{YOURSELF, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator(), 
    //     new CharacterManipulator({{"Energy", 20}}, {{"Potion", false}}));
    // new Effect("Fight",    
    //     new CharacterManipulator({}, {}, {{AFRAID, CharacterManipulator::MORE}}), 
    //     new CharacterManipulator({}, {}, {{ENEMY, CharacterManipulator::EQUAL}}),
    //     new CharacterManipulator());
    // new Effect("Hammer_Hit", "Hammer Hit",    
    //     new CharacterManipulator({{"Energy", 100}}, {{"Hammer", true}}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator({{"Energy", -50}}, {}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator({{"Energy", -10}}));
    // new Effect("Knife_Cut", "Cut with a knife",    
    //     new CharacterManipulator({}, {{"Knife", true}}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator({{"Energy", -20}}, {}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator());
    // new Effect("Magical_Hammer_Hit", "Magical Hammer Hit",    
    //     new CharacterManipulator({{"Energy", 100}, {"Faith", 40}}, {{"Magic_Wand", true}, {"Hammer", true}}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator({{"Energy", -60}, {"Faith", -15}}, {}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator({{"Energy", -10}, {"Faith", -10}}));
    // new Effect("Throw_a_stone", "Throw a stone",    
    //     new CharacterManipulator({}, {{"Stone", true}}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator({{"Energy", -10}}, {}, {{ENEMY, CharacterManipulator::EQUAL}}), 
    //     new CharacterManipulator({}, {{"Stone", false}}));

    

    // shadow->addSkill(new CharacterSkill("Energy", 100));
    // monk->addSkill(new CharacterSkill("Energy", 90));
    // villager->addSkill(new CharacterSkill("Energy", 100));
    // shadow->addSkill(new CharacterSkill("Faith", 100));
    // villager->addSkill(new CharacterSkill("Charisma", 50));
    // monk->addSkill(new CharacterSkill("Faith", 150));
    // knight->addSkill(new CharacterSkill("Energy", 150));
    // knight->addSkill(new CharacterSkill("Faith", 80));

    // monk->addItem("Magic_Wand");
    // shadow->addItem("Potion");
    // shadow->addItem("Hammer");
    // shadow->addItem("Stone");
    // monk->addItem("Stone");
    // villager->addItem("Stick");
    // knight->addItem("Hammer");
    // knight->addItem("Knife");

    // river->addCharacter(villager->getId());
    // city->addCharacter(monk->getId());
    // desert->addCharacter(shadow->getId());
    // walls->addCharacter(knight->getId());

    // desert->addItem("Stone");

    // desert->connectWith("Desert_River");
    // river->connectWith(desert->getId());
    // desert->connectWith("Pharos_Walls");
    // walls->connectWith(desert->getId());
    // desert->connectWith("Start");
    // city->connectWith("Pharos_Walls");
    // walls->connectWith(city->getId());
    
    // CharacterCreator* characterCreator = new CharacterCreator({{0, "Input name:"}, {1, "Input number of skill and desired number of points:"}, {2, "Pick one item:"}}, {{0, []() -> std::list<std::string> { return {}; }}, {1, Skill::getSkills}, {2, Item::getItems}});
    
    // InputHandler* menuHandler = new StatesHandler(menuData->getOptions()); 
    // InputHandler* emptyHandler = new StatesHandler(loadData->getOptions());
    // InputHandler* locHandler = new LocationHandler(desert);
    // InputHandler* characterHandler = new PlayerHandler(characterCreator);

    // GameState* menuState = new GameState("Main Menu", menuData, renderer, menuHandler);
    // GameState* gameState = new GameState("New Game", desert, renderer, locHandler);
    // GameState* loadState = new GameState("Load Game", loadData, renderer, emptyHandler);
    // GameState* dialogState = new GameState("Dialog State", NULL, renderer, emptyHandler);
    // GameState* fightState = new GameState("Fight State", NULL, renderer, NULL);
    // GameState* winState = new GameState("Win State", NULL, renderer, NULL);
    // GameState* dummyState = new GameState("Dummy State", NULL, renderer, NULL);

    // GameState* createState = new GameState("Create Character State", characterCreator, renderer, characterHandler);

    // manager->addState(menuState)->addState(gameState)->addState(loadState)->addState(dialogState)->addState(fightState)->addState(dummyState)->addState(createState)->addState(winState);

    // manager->push("Dummy State");
    // manager->top()->switchData(knight);
    // manager->top()->enter();

    signal( SIGTERM, signalHandler );
    signal( SIGINT, signalHandler );

    try {
        Game* game = new Game(new StateManager(new ConsoleProvider(), new ConsoleRenderer()), new FileManager());
        game->start(FileManager::src);
    } catch (Data* e) {
        ConsoleRenderer* renderer = new ConsoleRenderer();
        renderer->setMode(Renderer::ERROR);
        renderer->render(e);
        delete renderer;
    }

    // std::ofstream fileOutItem("saves/autoSave/items.src");
    // for(auto e : Item::getItems()) { 
    //     Item::getItem(e)->toFile(fileOutItem); }
    // fileOutItem.close();

    // std::ofstream fileOutSkill("saves/autoSave/skills.src");
    // for(auto e : Skill::getSkills()) { 
    //     Skill::getSkill(e)->toFile(fileOutSkill); 
    // }
    // fileOutSkill.close();

    // std::ofstream fileOutChar("saves/autoSave/characters.src");
    // for(auto e : Character::getCharacters()) { 
    //     Character::getCharacter(e)->toFile(fileOutChar); 
    // }
    // fileOutChar.close();

    // std::ofstream fileOutLoc("saves/autoSave/locations.src");
    // for(auto e : Location::getLocations()) { 
    //     Location::getLocation(e)->toFile(fileOutLoc); 
    // }
    // fileOutLoc.close();

    // std::ofstream fileOutEffects("saves/autoSave/effects.src");
    // for(auto e : Effect::getEffects()) {
    //     Effect::getEffect(e)->toFile(fileOutEffects);
    // }
    // fileOutEffects.close();
    clear();

    return 0;
}