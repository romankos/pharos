#ifndef locationHandler_h
#define locationHandler_h

#include <string>

#include "inputHandler.h"
#include "location.h"

/**
 * Class LocationHandler implements InputHandler.
 * Handles input in the context of Location Menu.
 */
class LocationHandler : public InputHandler {
private:
    Location* location;
public:

    LocationHandler(Location* location);
    virtual ~LocationHandler();

    virtual void handle(const std::string& data) const override;
};

#endif