#Author: Kostiantyn Romanov
#romankos@fit.cvut.cz

PROGRAM=romankos

CC=g++
CFLAGS=-std=c++11 -Wall -pedantic -Wextra -g -Wno-long-long -O0 -ggdb

all: compile doc

run: compile
	./$(PROGRAM) -o $@

compile: $(PROGRAM)

doc: src/army.cpp src/army.h src/attitude.cpp src/attitude.h src/character.cpp src/characterCreator.cpp src/characterCreator.h src/character.h src/characterManipulator.cpp src/characterManipulator.h src/clearable.cpp src/clearable.h src/consoleProvider.cpp src/consoleProvider.h src/consoleRenderer.cpp src/consoleRenderer.h src/data.h src/dialogHandler.cpp src/dialogHandler.h src/effect.cpp src/effect.h src/fight.cpp src/fight.h src/fightHandler.cpp src/fightHandler.h src/fileManager.cpp src/fileManager.h src/game.cpp src/game.h src/gameState.cpp src/gameState.h src/genericData.cpp src/genericData.h src/genericInputHandler.h src/identity.h src/inputHandler.h src/inputProvider.h src/item.cpp src/itemDeleteHandler.cpp src/itemDeleteHandler.h src/item.h src/itemHandler.cpp src/itemHandler.h src/location.cpp src/location.h src/locationHandler.cpp src/locationHandler.h src/main.cpp src/playerHandler.cpp src/playerHandler.h src/renderer.cpp src/renderer.h src/serializable.h src/skill.cpp src/skill.h src/stateManager.cpp src/stateManager.h
	doxygen Doxyfile
	
$(PROGRAM): objs/army.o objs/attitude.o objs/character.o objs/characterCreator.o objs/characterManipulator.o objs/clearable.o objs/consoleProvider.o objs/consoleRenderer.o objs/dialogHandler.o objs/effect.o objs/fight.o objs/fightHandler.o objs/fileManager.o objs/game.o objs/gameState.o objs/genericData.o objs/item.o objs/itemDeleteHandler.o objs/itemHandler.o objs/location.o objs/locationHandler.o objs/main.o objs/playerHandler.o objs/renderer.o objs/skill.o objs/stateManager.o
	$(CC) $(CFLAGS) $^ -o $@


objs/army.o: 		src/army.cpp src/army.h src/data.h src/clearable.h \
			src/character.h src/identity.h src/skill.h src/serializable.h \
			src/attitude.h src/item.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/attitude.o: 	src/attitude.cpp src/attitude.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/character.o: 	src/character.cpp src/character.h src/identity.h \
			src/clearable.h src/data.h src/skill.h src/serializable.h src/attitude.h \
			src/item.h src/game.h src/stateManager.h src/gameState.h src/renderer.h \
			src/inputHandler.h src/inputProvider.h src/fileManager.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/characterCreator.o: src/characterCreator.cpp src/characterCreator.h \
			src/data.h src/clearable.h src/character.h src/identity.h src/skill.h \
			src/serializable.h src/attitude.h src/item.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/characterManipulator.o: src/characterManipulator.cpp \
			src/characterManipulator.h src/data.h src/clearable.h src/attitude.h \
			src/location.h src/identity.h src/serializable.h src/character.h \
			src/skill.h src/item.h src/genericInputHandler.h src/inputHandler.h \
			src/game.h src/stateManager.h src/gameState.h src/renderer.h \
			src/inputProvider.h src/fileManager.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/clearable.o: 	src/clearable.cpp src/clearable.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
	
objs/consoleProvider.o: 	src/consoleProvider.cpp src/consoleProvider.h \
			src/inputProvider.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
	
objs/consoleRenderer.o: 	src/consoleRenderer.cpp src/consoleRenderer.h \
			src/renderer.h src/data.h src/clearable.h src/location.h src/identity.h \
			src/serializable.h src/character.h src/skill.h src/attitude.h src/item.h \
			src/genericInputHandler.h src/inputHandler.h src/game.h \
			src/stateManager.h src/gameState.h src/inputProvider.h src/fileManager.h \
			src/effect.h src/characterManipulator.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/dialogHandler.o: 	src/dialogHandler.cpp src/dialogHandler.h \
			src/inputHandler.h src/data.h src/clearable.h src/character.h \
			src/identity.h src/skill.h src/serializable.h src/attitude.h src/item.h \
			src/game.h src/stateManager.h src/gameState.h src/renderer.h \
			src/inputProvider.h src/fileManager.h src/effect.h \
			src/characterManipulator.h src/location.h src/genericInputHandler.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
	
objs/effect.o: 		src/effect.cpp src/effect.h src/data.h src/clearable.h \
			src/characterManipulator.h src/attitude.h src/location.h src/identity.h \
			src/serializable.h src/character.h src/skill.h src/item.h \
			src/genericInputHandler.h src/inputHandler.h src/game.h \
			src/stateManager.h src/gameState.h src/renderer.h src/inputProvider.h \
			src/fileManager.h src/genericData.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/fight.o: 		src/fight.cpp src/fight.h src/data.h src/clearable.h src/army.h \
			src/character.h src/identity.h src/skill.h src/serializable.h \
			src/attitude.h src/item.h src/location.h src/genericInputHandler.h \
			src/inputHandler.h src/effect.h src/characterManipulator.h src/game.h \
			src/stateManager.h src/gameState.h src/renderer.h src/inputProvider.h \
			src/fileManager.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/fightHandler.o: 	src/fightHandler.cpp src/fightHandler.h src/game.h \
			src/character.h src/identity.h src/clearable.h src/data.h src/skill.h \
			src/serializable.h src/attitude.h src/item.h src/stateManager.h \
			src/gameState.h src/renderer.h src/inputHandler.h src/inputProvider.h \
			src/fileManager.h src/fight.h src/army.h src/location.h \
			src/genericInputHandler.h src/effect.h src/characterManipulator.h \
			src/genericData.h src/itemHandler.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
	
objs/fileManager.o: 	src/fileManager.cpp src/fileManager.h src/genericData.h \
			src/data.h src/clearable.h src/item.h src/identity.h src/serializable.h \
			src/effect.h src/characterManipulator.h src/attitude.h src/location.h \
			src/character.h src/skill.h src/genericInputHandler.h src/inputHandler.h \
			src/game.h src/stateManager.h src/gameState.h src/renderer.h \
			src/inputProvider.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/game.o: 		src/game.cpp src/game.h src/character.h src/identity.h \
			src/clearable.h src/data.h src/skill.h src/serializable.h src/attitude.h \
			src/item.h src/stateManager.h src/gameState.h src/renderer.h \
			src/inputHandler.h src/inputProvider.h src/fileManager.h \
			src/characterCreator.h src/locationHandler.h src/location.h \
			src/genericInputHandler.h src/genericData.h src/playerHandler.h \
			src/consoleProvider.h src/consoleRenderer.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/gameState.o: 	src/gameState.cpp src/gameState.h src/data.h src/clearable.h \
			src/renderer.h src/inputHandler.h src/genericData.h src/game.h \
			src/character.h src/identity.h src/skill.h src/serializable.h \
			src/attitude.h src/item.h src/stateManager.h src/inputProvider.h \
			src/fileManager.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/genericData.o: 	src/genericData.cpp src/genericData.h src/data.h \
			src/clearable.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/item.o: 		src/item.cpp src/item.h src/data.h src/clearable.h src/identity.h \
			src/serializable.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/itemDeleteHandler.o: src/itemDeleteHandler.cpp src/itemDeleteHandler.h \
			src/inputHandler.h src/data.h src/clearable.h src/character.h \
			src/identity.h src/skill.h src/serializable.h src/attitude.h src/item.h \
			src/game.h src/stateManager.h src/gameState.h src/renderer.h \
			src/inputProvider.h src/fileManager.h src/location.h \
			src/genericInputHandler.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/itemHandler.o: 	src/itemHandler.cpp src/itemHandler.h src/inputHandler.h \
			src/data.h src/clearable.h src/character.h src/identity.h src/skill.h \
			src/serializable.h src/attitude.h src/item.h src/game.h \
			src/stateManager.h src/gameState.h src/renderer.h src/inputProvider.h \
			src/fileManager.h src/itemDeleteHandler.h src/genericData.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/location.o: 	src/location.cpp src/location.h src/identity.h \
			src/clearable.h src/data.h src/serializable.h src/character.h \
			src/skill.h src/attitude.h src/item.h src/genericInputHandler.h \
			src/inputHandler.h src/game.h src/stateManager.h src/gameState.h \
			src/renderer.h src/inputProvider.h src/fileManager.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/locationHandler.o: 	src/locationHandler.cpp src/locationHandler.h \
			src/inputHandler.h src/data.h src/clearable.h src/location.h \
			src/identity.h src/serializable.h src/character.h src/skill.h \
			src/attitude.h src/item.h src/genericInputHandler.h src/genericData.h \
			src/fightHandler.h src/game.h src/stateManager.h src/gameState.h \
			src/renderer.h src/inputProvider.h src/fileManager.h src/fight.h \
			src/army.h src/effect.h src/characterManipulator.h src/dialogHandler.h \
			src/itemDeleteHandler.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/main.o: 		src/main.cpp src/consoleProvider.h src/inputProvider.h \
			src/consoleRenderer.h src/renderer.h src/data.h src/clearable.h \
			src/stateManager.h src/gameState.h src/inputHandler.h src/location.h \
			src/identity.h src/serializable.h src/character.h src/skill.h \
			src/attitude.h src/item.h src/genericInputHandler.h \
			src/locationHandler.h src/genericData.h src/fight.h src/army.h \
			src/effect.h src/characterManipulator.h src/game.h src/fileManager.h \
			src/fightHandler.h src/characterCreator.h src/playerHandler.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/playerHandler.o: 	src/playerHandler.cpp src/playerHandler.h \
			src/inputHandler.h src/data.h src/clearable.h src/characterCreator.h \
			src/character.h src/identity.h src/skill.h src/serializable.h \
			src/attitude.h src/item.h src/genericData.h src/locationHandler.h \
			src/location.h src/genericInputHandler.h src/game.h src/stateManager.h \
			src/gameState.h src/renderer.h src/inputProvider.h src/fileManager.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
	
objs/renderer.o: 	src/renderer.cpp src/renderer.h src/data.h src/clearable.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/skill.o: 		src/skill.cpp src/skill.h src/data.h src/clearable.h \
			src/identity.h src/serializable.h src/genericData.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs/stateManager.o: 	src/stateManager.cpp src/stateManager.h src/gameState.h \
			src/data.h src/clearable.h src/renderer.h src/inputHandler.h \
			src/inputProvider.h src/genericInputHandler.h src/character.h \
			src/identity.h src/skill.h src/serializable.h src/attitude.h src/item.h \
			src/game.h src/fileManager.h | objs
			$(CC) $(CFLAGS) -c $< -o $@
			
objs: 
	mkdir objs
	
clean:
	rm -rf $(PROGRAM) objs/ doc/ 2>/dev/null
